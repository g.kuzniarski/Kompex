package com.kompex.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.kompex.backend.configuration.JpaConfiguration;

/**
 *
 * @author grzechu
 */
@Import(JpaConfiguration.class)
@SpringBootApplication(scanBasePackages={"com.kompex.backend"})
public class Kompex {
 
    public static void main(String[] args) {
        SpringApplication.run(Kompex.class, args);
    }
}
