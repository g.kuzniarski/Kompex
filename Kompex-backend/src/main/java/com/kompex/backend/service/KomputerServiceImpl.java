package com.kompex.backend.service;

import java.util.List;

import com.kompex.backend.model.Komputer;
import com.kompex.backend.repositories.KomputerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author grzechu
 */
@Service("komputerService")
@Transactional
public class KomputerServiceImpl implements KomputerService {

    @Autowired
    private KomputerRepository komputerRepository;

    @Override
    public Komputer findById(Long id) {
        return komputerRepository.findOne(id);
    }

    @Override
    public void saveKomputer(Komputer komputer) {
        komputerRepository.save(komputer);
    }

    @Override
    public void updateKomputer(Komputer komputer) {
        saveKomputer(komputer);
    }

    @Override
    public void deleteKomputerById(Long id) {
        komputerRepository.delete(id);
    }

    @Override
    public void deleteAllKomputers() {
        komputerRepository.deleteAll();
    }

    @Override
    public List<Komputer> findAllKomputers() {
        return komputerRepository.findAll();
    }

    @Override
    public boolean isKomputerExist(Komputer komputer) {
        return findById(komputer.getIdKomputer()) != null;
    }

}
