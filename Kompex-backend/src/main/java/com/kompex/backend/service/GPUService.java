package com.kompex.backend.service;

import com.kompex.backend.model.GPU;
import java.util.List;

/**
 *
 * @author grzechu
 */
public interface GPUService {

    GPU findById(Long id);

    GPU findByNazwa(String name);

    void saveGPU(GPU gPU);

    void updateGPU(GPU gPU);

    void deleteGPUById(Long id);

    void deleteAllGPUs();

    List<GPU> findAllGPUs();

    boolean isGPUExist(GPU gPU);
}
