package com.kompex.backend.service;

import java.util.List;

import com.kompex.backend.model.CPU;
import com.kompex.backend.repositories.CPURepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author grzechu
 */
@Service("cPUService")
@Transactional
public class CPUServiceImpl implements CPUService {

    @Autowired
    private CPURepository cPURepository;

    @Override
    public CPU findById(Long id) {
        return cPURepository.findOne(id);
    }

    @Override
    public CPU findByNazwa(String name) {
        return cPURepository.findByNazwa(name);
    }

    @Override
    public void saveCPU(CPU cPU) {
        cPURepository.save(cPU);
    }

    @Override
    public void updateCPU(CPU cPU) {
        saveCPU(cPU);
    }

    @Override
    public void deleteCPUById(Long id) {
        cPURepository.delete(id);
    }

    @Override
    public void deleteAllCPUs() {
        cPURepository.deleteAll();
    }

    @Override
    public List<CPU> findAllCPUs() {
        return cPURepository.findAll();
    }

    @Override
    public boolean isCPUExist(CPU cPU) {
        return findById(cPU.getIdCPU()) != null;
    }

}
