package com.kompex.backend.service;

import com.kompex.backend.model.CPU;
import java.util.List;

/**
 *
 * @author grzechu
 */
public interface CPUService {

    CPU findById(Long id);

    CPU findByNazwa(String name);

    void saveCPU(CPU cPU);

    void updateCPU(CPU cPU);

    void deleteCPUById(Long id);

    void deleteAllCPUs();

    List<CPU> findAllCPUs();

    boolean isCPUExist(CPU cPU);
}
