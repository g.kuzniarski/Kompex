package com.kompex.backend.service;

import com.kompex.backend.model.Zamowienie;
import com.kompex.backend.repositories.ZamowienieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * @author grzechu
 */
@Service("zamowienieService")
@Transactional
public class ZamowienieServiceImpl implements ZamowienieService {

    @Autowired
    private ZamowienieRepository zamowienieRepository;

    @Override
    public Zamowienie findById(Long id) {
        return zamowienieRepository.findOne(id);
    }

    @Override
    public void saveZamowienie(Zamowienie zamowienie) {
        zamowienieRepository.save(zamowienie);
    }

    @Override
    public void updateZamowienie(Zamowienie zamowienie) {
        saveZamowienie(zamowienie);
    }

    @Override
    public void deleteZamowienieById(Long id) {
        zamowienieRepository.delete(id);
    }

    @Override
    public void deleteAllZamowienies() {
        zamowienieRepository.deleteAll();
    }

    @Override
    public List<Zamowienie> findAllZamowienies() {
        return zamowienieRepository.findAll();
    }

    @Override
    public boolean isZamowienieExist(Zamowienie zamowienie) {
        return findById(zamowienie.getIdZamowienie()) != null;
    }

}
