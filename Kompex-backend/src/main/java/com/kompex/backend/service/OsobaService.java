package com.kompex.backend.service;

import com.kompex.backend.model.Osoba;
import java.util.List;

/**
 *
 * @author grzechu
 */
public interface OsobaService {

    Osoba findById(Long id);

    void saveOsoba(Osoba osoba);

    void updateOsoba(Osoba osoba);

    void deleteOsobaById(Long id);

    void deleteAllOsobas();

    List<Osoba> findAllOsobas();

    boolean isOsobaExist(Osoba osoba);

    Osoba findByNazwisko(String nazwisko);
}
