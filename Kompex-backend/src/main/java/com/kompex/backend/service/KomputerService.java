package com.kompex.backend.service;

import com.kompex.backend.model.Komputer;
import java.util.List;

/**
 *
 * @author grzechu
 */
public interface KomputerService {

    Komputer findById(Long id);

    void saveKomputer(Komputer komputer);

    void updateKomputer(Komputer komputer);

    void deleteKomputerById(Long id);

    void deleteAllKomputers();

    List<Komputer> findAllKomputers();

    boolean isKomputerExist(Komputer komputer);
}
