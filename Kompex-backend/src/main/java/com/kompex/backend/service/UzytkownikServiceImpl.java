package com.kompex.backend.service;

import com.kompex.backend.model.Uzytkownik;
import com.kompex.backend.repositories.UzytkownikRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author grzechu
 */
@Service("uzytkownikService")
@Transactional
public class UzytkownikServiceImpl implements UzytkownikService {

    @Autowired
    private UzytkownikRepository uzytkownikRepository;

    @Override
    public Uzytkownik findById(Long id) {
        return uzytkownikRepository.findOne(id);
    }

    @Override
    public Uzytkownik findByUsername(String username) {
        return uzytkownikRepository.findByUsername(username);
    }

    @Override
    public void saveUzytkownik(Uzytkownik uzytkownik) {
        uzytkownikRepository.save(uzytkownik);
    }

    @Override
    public void updateUzytkownik(Uzytkownik uzytkownik) {
        saveUzytkownik(uzytkownik);
    }

    @Override
    public void deleteUzytkownikById(Long id) {
        uzytkownikRepository.delete(id);
    }

    @Override
    public void deleteAllUzytkowniks() {
        uzytkownikRepository.deleteAll();
    }

    @Override
    public List<Uzytkownik> findAllUzytkowniks() {
        return uzytkownikRepository.findAll();
    }

    @Override
    public boolean isUzytkownikExist(Uzytkownik uzytkownik) {
        return findById(uzytkownik.getIdUzytkownik()) != null;
    }
}
