package com.kompex.backend.service;

import java.util.List;

import com.kompex.backend.model.Dysk;
import com.kompex.backend.repositories.DyskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author grzechu
 */
@Service("dyskService")
@Transactional
public class DyskServiceImpl implements DyskService {

    @Autowired
    private DyskRepository dyskRepository;

    @Override
    public Dysk findById(Long id) {
        return dyskRepository.findOne(id);
    }

    @Override
    public Dysk findByNazwa(String name) {
        return dyskRepository.findByNazwa(name);
    }

    @Override
    public void saveDysk(Dysk dysk) {
        dyskRepository.save(dysk);
    }

    @Override
    public void updateDysk(Dysk dysk) {
        saveDysk(dysk);
    }

    @Override
    public void deleteDyskById(Long id) {
        dyskRepository.delete(id);
    }

    @Override
    public void deleteAllDysks() {
        dyskRepository.deleteAll();
    }

    @Override
    public List<Dysk> findAllDysks() {
        return dyskRepository.findAll();
    }

    @Override
    public boolean isDyskExist(Dysk dysk) {
        return findById(dysk.getIdDysk()) != null;
    }

}
