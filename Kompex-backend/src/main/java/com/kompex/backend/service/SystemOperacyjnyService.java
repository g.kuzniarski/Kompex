package com.kompex.backend.service;

import com.kompex.backend.model.SystemOperacyjny;
import java.util.List;

/**
 *
 * @author grzechu
 */
public interface SystemOperacyjnyService {

    SystemOperacyjny findById(Long id);

    SystemOperacyjny findByNazwa(String name);

    void saveSystemOperacyjny(SystemOperacyjny systemOperacyjny);

    void updateSystemOperacyjny(SystemOperacyjny systemOperacyjny);

    void deleteSystemOperacyjnyById(Long id);

    void deleteAllSystemOperacyjnys();

    List<SystemOperacyjny> findAllSystemOperacyjnys();

    boolean isSystemOperacyjnyExist(SystemOperacyjny systemOperacyjny);
}
