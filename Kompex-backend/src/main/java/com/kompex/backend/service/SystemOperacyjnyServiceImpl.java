package com.kompex.backend.service;

import java.util.List;

import com.kompex.backend.model.SystemOperacyjny;
import com.kompex.backend.repositories.SystemOperacyjnyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author grzechu
 */
@Service("systemOperacyjnyService")
@Transactional
public class SystemOperacyjnyServiceImpl implements SystemOperacyjnyService {

    @Autowired
    private SystemOperacyjnyRepository systemOperacyjnyRepository;

    @Override
    public SystemOperacyjny findById(Long id) {
        return systemOperacyjnyRepository.findOne(id);
    }

    @Override
    public SystemOperacyjny findByNazwa(String name) {
        return systemOperacyjnyRepository.findByNazwa(name);
    }

    @Override
    public void saveSystemOperacyjny(SystemOperacyjny systemOperacyjny) {
        systemOperacyjnyRepository.save(systemOperacyjny);
    }

    @Override
    public void updateSystemOperacyjny(SystemOperacyjny systemOperacyjny) {
        saveSystemOperacyjny(systemOperacyjny);
    }

    @Override
    public void deleteSystemOperacyjnyById(Long id) {
        systemOperacyjnyRepository.delete(id);
    }

    @Override
    public void deleteAllSystemOperacyjnys() {
        systemOperacyjnyRepository.deleteAll();
    }

    @Override
    public List<SystemOperacyjny> findAllSystemOperacyjnys() {
        return systemOperacyjnyRepository.findAll();
    }

    @Override
    public boolean isSystemOperacyjnyExist(SystemOperacyjny systemOperacyjny) {
        return findById(systemOperacyjny.getIdSystemOperacyjny()) != null;
    }

}
