package com.kompex.backend.service;

import com.kompex.backend.model.Zasilacz;

import java.util.List;

/**
 *
 * @author grzechu
 */
public interface ZasilaczService {

    Zasilacz findById(Long id);

    Zasilacz findByNazwa(String name);

    void saveZasilacz(Zasilacz zasilacz);

    void updateZasilacz(Zasilacz zasilacz);

    void deleteZasilaczById(Long id);

    void deleteAllZasilaczs();

    List<Zasilacz> findAllZasilaczs();

    boolean isZasilaczExist(Zasilacz zasilacz);
}
