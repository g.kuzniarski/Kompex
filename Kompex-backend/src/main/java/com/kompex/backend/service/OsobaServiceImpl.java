package com.kompex.backend.service;

import java.util.List;

import com.kompex.backend.model.Osoba;
import com.kompex.backend.repositories.OsobaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author grzechu
 */
@Service("osobaService")
@Transactional
public class OsobaServiceImpl implements OsobaService {

    @Autowired
    private OsobaRepository osobaRepository;

    @Override
    public Osoba findById(Long id) {
        return osobaRepository.findOne(id);
    }

    @Override
    public void saveOsoba(Osoba osoba) {
        osobaRepository.save(osoba);
    }

    @Override
    public void updateOsoba(Osoba osoba) {
        saveOsoba(osoba);
    }

    @Override
    public void deleteOsobaById(Long id) {
        osobaRepository.delete(id);
    }

    @Override
    public void deleteAllOsobas() {
        osobaRepository.deleteAll();
    }

    @Override
    public List<Osoba> findAllOsobas() {
        return osobaRepository.findAll();
    }

    @Override
    public boolean isOsobaExist(Osoba osoba) {
        return findById(osoba.getIdOsoba()) != null;
    }

    @Override
    public Osoba findByNazwisko(String nazwisko) {
        return osobaRepository.findByNazwisko(nazwisko);
    }

}
