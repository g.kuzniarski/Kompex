package com.kompex.backend.service;

import com.kompex.backend.model.RAM;

import java.util.List;

/**
 *
 * @author grzechu
 */
public interface RAMService {

    RAM findById(Long id);

    RAM findByNazwa(String name);

    void saveRAM(RAM rAM);

    void updateRAM(RAM rAM);

    void deleteRAMById(Long id);

    void deleteAllRAMs();

    List<RAM> findAllRAMs();

    boolean isRAMExist(RAM rAM);
}
