package com.kompex.backend.service;

import com.kompex.backend.model.Uzytkownik;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

/**
 *
 * @author grzechu
 */
public interface UzytkownikService {

    Uzytkownik findById(Long id);

    Uzytkownik findByUsername(String login);

    void saveUzytkownik(Uzytkownik uzytkownik);

    void updateUzytkownik(Uzytkownik uzytkownik);

    void deleteUzytkownikById(Long id);

    void deleteAllUzytkowniks();

    List<Uzytkownik> findAllUzytkowniks();

    boolean isUzytkownikExist(Uzytkownik uzytkownik);
}
