package com.kompex.backend.service;

import com.kompex.backend.model.RAM;
import com.kompex.backend.repositories.RAMRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * @author grzechu
 */
@Service("rAMService")
@Transactional
public class RAMServiceImpl implements RAMService {

    @Autowired
    private RAMRepository rAMRepository;

    @Override
    public RAM findById(Long id) {
        return rAMRepository.findOne(id);
    }

    @Override
    public RAM findByNazwa(String name) {
        return rAMRepository.findByNazwa(name);
    }

    @Override
    public void saveRAM(RAM rAM) {
        rAMRepository.save(rAM);
    }

    @Override
    public void updateRAM(RAM rAM) {
        saveRAM(rAM);
    }

    @Override
    public void deleteRAMById(Long id) {
        rAMRepository.delete(id);
    }

    @Override
    public void deleteAllRAMs() {
        rAMRepository.deleteAll();
    }

    @Override
    public List<RAM> findAllRAMs() {
        return rAMRepository.findAll();
    }

    @Override
    public boolean isRAMExist(RAM rAM) {
        return findById(rAM.getIdRAM()) != null;
    }

}
