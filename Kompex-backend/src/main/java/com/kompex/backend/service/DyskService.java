package com.kompex.backend.service;

import com.kompex.backend.model.Dysk;
import java.util.List;

/**
 *
 * @author grzechu
 */
public interface DyskService {

    Dysk findById(Long id);

    Dysk findByNazwa(String name);

    void saveDysk(Dysk dysk);

    void updateDysk(Dysk dysk);

    void deleteDyskById(Long id);

    void deleteAllDysks();

    List<Dysk> findAllDysks();

    boolean isDyskExist(Dysk dysk);
}
