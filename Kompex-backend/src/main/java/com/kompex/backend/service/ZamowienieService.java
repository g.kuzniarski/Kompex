package com.kompex.backend.service;

import com.kompex.backend.model.Zamowienie;

import java.util.List;

/**
 *
 * @author grzechu
 */
public interface ZamowienieService {

    Zamowienie findById(Long id);

    void saveZamowienie(Zamowienie zamowienie);

    void updateZamowienie(Zamowienie zamowienie);

    void deleteZamowienieById(Long id);

    void deleteAllZamowienies();

    List<Zamowienie> findAllZamowienies();

    boolean isZamowienieExist(Zamowienie zamowienie);
}
