package com.kompex.backend.service;

import java.util.List;

import com.kompex.backend.model.GPU;
import com.kompex.backend.repositories.GPURepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author grzechu
 */
@Service("gPUService")
@Transactional
public class GPUServiceImpl implements GPUService {

    @Autowired
    private GPURepository gPURepository;

    @Override
    public GPU findById(Long id) {
        return gPURepository.findOne(id);
    }

    @Override
    public GPU findByNazwa(String name) {
        return gPURepository.findByNazwa(name);
    }

    @Override
    public void saveGPU(GPU gPU) {
        gPURepository.save(gPU);
    }

    @Override
    public void updateGPU(GPU gPU) {
        saveGPU(gPU);
    }

    @Override
    public void deleteGPUById(Long id) {
        gPURepository.delete(id);
    }

    @Override
    public void deleteAllGPUs() {
        gPURepository.deleteAll();
    }

    @Override
    public List<GPU> findAllGPUs() {
        return gPURepository.findAll();
    }

    @Override
    public boolean isGPUExist(GPU gPU) {
        return findById(gPU.getIdGPU()) != null;
    }

}
