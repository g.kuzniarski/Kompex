package com.kompex.backend.service;

import com.kompex.backend.model.Zasilacz;
import com.kompex.backend.repositories.ZasilaczRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * @author grzechu
 */
@Service("zasilaczService")
@Transactional
public class ZasilaczServiceImpl implements ZasilaczService {

    @Autowired
    private ZasilaczRepository zasilaczRepository;

    @Override
    public Zasilacz findById(Long id) {
        return zasilaczRepository.findOne(id);
    }

    @Override
    public Zasilacz findByNazwa(String name) {
        return zasilaczRepository.findByNazwa(name);
    }

    @Override
    public void saveZasilacz(Zasilacz zasilacz) {
        zasilaczRepository.save(zasilacz);
    }

    @Override
    public void updateZasilacz(Zasilacz zasilacz) {
        saveZasilacz(zasilacz);
    }

    @Override
    public void deleteZasilaczById(Long id) {
        zasilaczRepository.delete(id);
    }

    @Override
    public void deleteAllZasilaczs() {
        zasilaczRepository.deleteAll();
    }

    @Override
    public List<Zasilacz> findAllZasilaczs() {
        return zasilaczRepository.findAll();
    }

    @Override
    public boolean isZasilaczExist(Zasilacz zasilacz) {
        return findById(zasilacz.getIdZasilacz()) != null;
    }

}
