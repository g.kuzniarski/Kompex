/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kompex.backend.model;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Grzechu
 */
@Entity
@Table(name = "Zasilacz")
public class Zasilacz implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idZasilacz;

    @NotEmpty
    @Column(name = "Nazwa", nullable = false)
    private String nazwa;

    @NotEmpty
    @Column(name = "Producent", nullable = false)
    private String producent;

    @NotEmpty
    @Column(name = "Cena", nullable = false)
    private int cena;

    @Column(name = "Moc")
    private int moc;

    @Column(name = "ImageName")
    private String imageName;

    public Long getIdZasilacz() {
        return idZasilacz;
    }

    public void setIdZasilacz(Long idZasilacz) {
        this.idZasilacz = idZasilacz;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getProducent() {
        return producent;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public int getMoc() {
        return moc;
    }

    public void setMoc(int moc) {
        this.moc = moc;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
