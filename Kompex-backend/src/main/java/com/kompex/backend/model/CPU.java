package com.kompex.backend.model;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author grzechu
 */
@Entity
@Table(name = "CPU")
public class CPU implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCPU;

    @NotEmpty
    @Column(name = "Nazwa", nullable = false, length = 45)
    private String nazwa;

    @NotEmpty
    @Column(name = "Producent", nullable = false, length = 45)
    private String producent;

    @NotEmpty
    @Column(name = "Cena", nullable = false)
    private int cena;
    
    @Column(name = "Typ_gniazda")
    private String typGniazda;

    @Column(name = "Ilosc_rdzeni")
    private int iloscRdzeni;

    @Column(name = "Czetotliwosc_taktowania")
    private int czestotliwoscTaktowania;

    @Column(name = "Pojemnosc_pamieci_cache_L2")
    private int pojemnoscL2;

    @Column(name = "ImageName")
    private String imageName;

    public Long getIdCPU() {
        return idCPU;
    }

    public void setIdCPU(Long idCPU) {
        this.idCPU = idCPU;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getProducent() {
        return producent;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public String getTypGniazda() {
        return typGniazda;
    }

    public void setTypGniazda(String typGniazda) {
        this.typGniazda = typGniazda;
    }

    public int getIloscRdzeni() {
        return iloscRdzeni;
    }

    public void setIloscRdzeni(int iloscRdzeni) {
        this.iloscRdzeni = iloscRdzeni;
    }

    public int getCzestotliwoscTaktowania() {
        return czestotliwoscTaktowania;
    }

    public void setCzestotliwoscTaktowania(int czestotliwoscTaktowania) {
        this.czestotliwoscTaktowania = czestotliwoscTaktowania;
    }

    public int getPojemnoscL2() {
        return pojemnoscL2;
    }

    public void setPojemnoscL2(int pojemnoscL2) {
        this.pojemnoscL2 = pojemnoscL2;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
