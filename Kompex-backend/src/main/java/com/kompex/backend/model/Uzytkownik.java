package com.kompex.backend.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

@Entity
@Table(name = "Uzytkownik")
public class Uzytkownik {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUzytkownik;

    @NotEmpty
    @Column(name = "Username", unique = true)
    private String username;

    @Column(name = "Password")
    private String password;

    @Column(name = "Enabled", nullable = false)
    private boolean enabled;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idOsoba")
    private Osoba osoba;

    @Column(name = "Rola")
    @Enumerated(EnumType.STRING)
    private dicRola rola;

    public Uzytkownik() {
    }

    public Long getIdUzytkownik() {
        return idUzytkownik;
    }

    public void setIdUzytkownik(Long idUzytkownik) {
        this.idUzytkownik = idUzytkownik;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Osoba getOsoba() {
        return osoba;
    }

    public void setOsoba(Osoba osoba) {
        this.osoba = osoba;
    }

    public dicRola getRola() {
        return rola;
    }

    public void setRola(dicRola rola) {
        this.rola = rola;
    }
}
