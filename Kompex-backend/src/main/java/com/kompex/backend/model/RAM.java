package com.kompex.backend.model;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Grzechu
 */
@Entity
@Table(name = "RAM")
public class RAM implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idRAM;

    @NotEmpty
    @Column(name = "Nazwa", nullable = false)
    private String nazwa;

    @NotEmpty
    @Column(name = "Producent", nullable = false)
    private String producent;

    @NotEmpty
    @Column(name = "Cena", nullable = false)
    private int cena;

    @Column(name = "Pojemnosc")
    private int pojemnosc;

    @Column(name = "Typ_pamieci")
    @Enumerated(EnumType.STRING)
    private dicTypRAM typPamieci;

    @Column(name = "Czestotliwosc")
    private int czestotliwosc;

    @Column(name = "ImageName")
    private String imageName;

    public Long getIdRAM() {
        return idRAM;
    }

    public void setIdRAM(Long idRAM) {
        this.idRAM = idRAM;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getProducent() {
        return producent;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public int getPojemnosc() {
        return pojemnosc;
    }

    public void setPojemnosc(int pojemnosc) {
        this.pojemnosc = pojemnosc;
    }

    public dicTypRAM getTypPamieci() {
        return typPamieci;
    }

    public void setTypPamieci(dicTypRAM typPamieci) {
        this.typPamieci = typPamieci;
    }

    public int getCzestotliwosc() {
        return czestotliwosc;
    }

    public void setCzestotliwosc(int czestotliwosc) {
        this.czestotliwosc = czestotliwosc;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
