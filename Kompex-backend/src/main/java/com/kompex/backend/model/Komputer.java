package com.kompex.backend.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Komputer")
public class Komputer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idKomputer;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "idCPU")
    private CPU cpu;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "idDysk")
    private Dysk dysk1;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "idGPU")
    private GPU gpu;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "idRAM")
    private RAM ram;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "idSystemOperacyjny")
    private SystemOperacyjny systemOperacyjny;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "idZasilacz")
    private Zasilacz zasilacz;

    public long getIdKomputer() {
        return idKomputer;
    }

    public void setIdKomputer(long idKomputer) {
        this.idKomputer = idKomputer;
    }

    public CPU getCpu() {
        return cpu;
    }

    public void setCpu(CPU cpu) {
        this.cpu = cpu;
    }

    public Dysk getDysk1() {
        return dysk1;
    }

    public GPU getGpu() {
        return gpu;
    }

    public void setGpu(GPU gpu) {
        this.gpu = gpu;
    }

    public RAM getRam() {
        return ram;
    }

    public void setRam(RAM ram) {
        this.ram = ram;
    }

    public SystemOperacyjny getSystemOperacyjny() {
        return systemOperacyjny;
    }

    public void setSystemOperacyjny(SystemOperacyjny systemOperacyjny) {
        this.systemOperacyjny = systemOperacyjny;
    }

    public Zasilacz getZasilacz() {
        return zasilacz;
    }

    public void setZasilacz(Zasilacz zasilacz) {
        this.zasilacz = zasilacz;
    }
}
