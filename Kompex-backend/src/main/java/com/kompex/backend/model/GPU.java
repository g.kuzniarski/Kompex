package com.kompex.backend.model;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author grzechu
 */
@Entity
@Table(name = "GPU")
public class GPU implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idGPU;

    @NotEmpty
    @Column(name = "Nazwa", nullable = false)
    private String nazwa;

    @NotEmpty
    @Column(name = "Producent", nullable = false)
    private String producent;

    @NotEmpty
    @Column(name = "Cena", nullable = false)
    private int cena;

    @Column(name = "Pamiec")
    private int pamiec;

    @Column(name = "Typ_pamieci")
    @Enumerated(EnumType.STRING)
    private dicTypRAM typPamieci;

    @Column(name = "Taktowanie")
    private int taktowanie;

    @Column(name = "Typ_zlacza")
    @Enumerated(EnumType.STRING)
    private dicTypGPU typZlacza;

    @Column(name = "ImageName")
    private String imageName;

    public Long getIdGPU() {
        return idGPU;
    }

    public void setIdGPU(Long idGPU) {
        this.idGPU = idGPU;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getProducent() {
        return producent;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public int getPamiec() {
        return pamiec;
    }

    public void setPamiec(int pamiec) {
        this.pamiec = pamiec;
    }

    public dicTypRAM getTypPamieci() {
        return typPamieci;
    }

    public void setTypPamieci(String dicTypRAM) {
        this.typPamieci = typPamieci;
    }

    public int getTaktowanie() {
        return taktowanie;
    }

    public void setTaktowanie(int taktowanie) {
        this.taktowanie = taktowanie;
    }

    public dicTypGPU getTypZlacza() {
        return typZlacza;
    }

    public void setTypZlacza(dicTypGPU typZlacza) {
        this.typZlacza = typZlacza;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
