package com.kompex.backend.model;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author grzechu
 */
@Entity
@Table(name = "SystemOperacyjny")
public class SystemOperacyjny implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idSystemOperacyjny;

    @NotEmpty
    @Column(name = "Nazwa", nullable = false, length = 45)
    private String nazwa;

    @NotEmpty
    @Column(name = "Producent", nullable = false, length = 45)
    private String producent;

    @NotEmpty
    @Column(name = "Cena", nullable = false)
    private int cena;

    @Column(name = "ImageName")
    private String imageName;

    public Long getIdSystemOperacyjny() {
        return idSystemOperacyjny;
    }

    public void setIdSystemOperacyjny(Long idSystemOperacyjny) {
        this.idSystemOperacyjny = idSystemOperacyjny;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getProducent() {
        return producent;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
