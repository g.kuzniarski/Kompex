package com.kompex.backend.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "Zamowienie")
public class Zamowienie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idZamowienie;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "komputer_idKomputer", nullable = false)
    private Komputer komputer;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "uzytkownik_idUzytkownik", nullable = false)
    private Uzytkownik uzytkownik;

    @Column(name = "Data")
    @Temporal(TemporalType.DATE)
    private Date data;

    public Long getIdZamowienie() {
        return idZamowienie;
    }

    public void setIdZamowienie(Long idZamowienie) {
        this.idZamowienie = idZamowienie;
    }

    public Komputer getKomputer() {
        return komputer;
    }

    public void setKomputer(Komputer komputer) {
        this.komputer = komputer;
    }

    public Uzytkownik getUzytkownik() {
        return uzytkownik;
    }

    public void setUzytkownik(Uzytkownik uzytkownik) {
        this.uzytkownik = uzytkownik;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
