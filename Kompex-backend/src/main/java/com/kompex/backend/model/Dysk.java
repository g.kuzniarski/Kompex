/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kompex.backend.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Grzechu
 */
@Entity
@Table(name = "Dysk")
public class Dysk implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDysk;

    @NotEmpty
    @Column(name = "Nazwa", nullable = false)
    private String nazwa;

    @NotEmpty
    @Column(name = "Producent", nullable = false)
    private String producent;

    @NotEmpty
    @Column(name = "Cena", nullable = false)
    private int cena;

    @Column(name = "Pojemnosc")
    private int pojemnosc;

    @Column(name = "Typ")
    @Enumerated(EnumType.STRING)
    private dicTypDysk typ;

    @Column(name = "Interfejs")
    private String interfejs;

    @Column(name = "ImageName")
    private String imageName;

    public Long getIdDysk() {
        return idDysk;
    }

    public void setIdDysk(Long idDysk) {
        this.idDysk = idDysk;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getProducent() {
        return producent;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public int getPojemnosc() {
        return pojemnosc;
    }

    public void setPojemnosc(int pojemnosc) {
        this.pojemnosc = pojemnosc;
    }

    public dicTypDysk getTyp() {
        return typ;
    }

    public void setTyp(dicTypDysk typ) {
        this.typ = typ;
    }

    public String getInterfejs() {
        return interfejs;
    }

    public void setInterfejs(String interfejs) {
        this.interfejs = interfejs;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
