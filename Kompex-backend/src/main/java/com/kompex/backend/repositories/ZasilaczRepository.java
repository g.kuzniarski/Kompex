package com.kompex.backend.repositories;

import com.kompex.backend.model.Zasilacz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author grzechu
 */
@Repository
public interface ZasilaczRepository extends JpaRepository<Zasilacz, Long> {

    Zasilacz findByNazwa(String name);

}
