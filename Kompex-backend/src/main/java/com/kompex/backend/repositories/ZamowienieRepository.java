package com.kompex.backend.repositories;

import com.kompex.backend.model.Zamowienie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author grzechu
 */
@Repository
public interface ZamowienieRepository extends JpaRepository<Zamowienie, Long> {
}
