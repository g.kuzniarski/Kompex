package com.kompex.backend.repositories;

import com.kompex.backend.model.SystemOperacyjny;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author grzechu
 */
@Repository
public interface SystemOperacyjnyRepository extends JpaRepository<SystemOperacyjny, Long> {

    SystemOperacyjny findByNazwa(String name);

}
