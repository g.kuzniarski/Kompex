package com.kompex.backend.repositories;

import com.kompex.backend.model.GPU;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author grzechu
 */
@Repository
public interface GPURepository extends JpaRepository<GPU, Long> {

    GPU findByNazwa(String name);

}
