package com.kompex.backend.repositories;

import com.kompex.backend.model.Osoba;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author grzechu
 */
@Repository
public interface OsobaRepository extends JpaRepository<Osoba, Long> {
    Osoba findByNazwisko(String nazwisko);
}
