package com.kompex.backend.repositories;

import com.kompex.backend.model.Dysk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author grzechu
 */
@Repository
public interface DyskRepository extends JpaRepository<Dysk, Long> {

    Dysk findByNazwa(String name);

}
