package com.kompex.backend.repositories;

import com.kompex.backend.model.Komputer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author grzechu
 */
@Repository
public interface KomputerRepository extends JpaRepository<Komputer, Long> {
}
