package com.kompex.backend.repositories;

import com.kompex.backend.model.CPU;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author grzechu
 */
@Repository
public interface CPURepository extends JpaRepository<CPU, Long> {

    CPU findByNazwa(String name);

}
