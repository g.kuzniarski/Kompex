package com.kompex.backend.repositories;

import com.kompex.backend.model.Uzytkownik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author grzechu
 */
@Repository
public interface UzytkownikRepository extends JpaRepository<Uzytkownik, Long> {
    Uzytkownik findByUsername(String login);
}
