package com.kompex.backend.repositories;

import com.kompex.backend.model.RAM;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author grzechu
 */
@Repository
public interface RAMRepository extends JpaRepository<RAM, Long> {

    RAM findByNazwa(String name);

}
