package com.kompex.backend.controller;

import com.kompex.backend.model.Uzytkownik;
import com.kompex.backend.service.UzytkownikService;
import com.kompex.backend.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * @author grzechu
 */
@RestController
@RequestMapping("/api")
public class UzytkownikController {

    public static final Logger logger = LoggerFactory.getLogger(UzytkownikController.class);

    @Autowired
    UzytkownikService uzytkownikService;

    @RequestMapping(value = "/uzytkowniks/", method = RequestMethod.GET)
    public ResponseEntity<List<Uzytkownik>> listAllUzytkowniks() {
        List<Uzytkownik> uzytkowniks = uzytkownikService.findAllUzytkowniks();
        if (uzytkowniks.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(uzytkowniks, HttpStatus.OK);
    }

    @RequestMapping(value = "/uzytkownik/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getUzytkownik(@PathVariable("id") long id) {
        logger.info("Fetching Uzytkownik with id {}", id);
        Uzytkownik uzytkownik = uzytkownikService.findById(id);
        if (uzytkownik == null) {
            logger.error("Uzytkownik with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Uzytkownik with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Uzytkownik>(uzytkownik, HttpStatus.OK);
    }

    @RequestMapping(value = "/uzytkownik/", method = RequestMethod.POST)
    public ResponseEntity<?> createUzytkownik(@RequestBody Uzytkownik uzytkownik, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Uzytkownik : {}", uzytkownik);

        uzytkownikService.saveUzytkownik(uzytkownik);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/uzytkownik/{id}").buildAndExpand(uzytkownik.getIdUzytkownik()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/uzytkownik/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateUzytkownik(@PathVariable("id") long id, @RequestBody Uzytkownik uzytkownik) {
        logger.info("Updating Uzytkownik with id {}", id);

        Uzytkownik currentUzytkownik = uzytkownikService.findById(id);

        if (currentUzytkownik == null) {
            logger.error("Unable to update. Uzytkownik with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. Uzytkownik with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        uzytkownikService.updateUzytkownik(uzytkownik);
        return new ResponseEntity<Uzytkownik>(uzytkownik, HttpStatus.OK);
    }

    @RequestMapping(value = "/uzytkownik/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteUzytkownik(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting Uzytkownik with id {}", id);

        Uzytkownik uzytkownik = uzytkownikService.findById(id);
        if (uzytkownik == null) {
            logger.error("Unable to delete. Uzytkownik with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to delete. Uzytkownik with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        uzytkownikService.deleteUzytkownikById(id);
        return new ResponseEntity<Uzytkownik>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/uzytkownik/", method = RequestMethod.DELETE)
    public ResponseEntity<Uzytkownik> deleteAllUzytkowniks() {
        logger.info("Deleting All Uzytkowniks");

        uzytkownikService.deleteAllUzytkowniks();
        return new ResponseEntity<Uzytkownik>(HttpStatus.NO_CONTENT);
    }

}
