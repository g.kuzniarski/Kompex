package com.kompex.backend.controller;

import com.kompex.backend.model.Osoba;
import com.kompex.backend.model.Uzytkownik;
import com.kompex.backend.model.Zamowienie;
import com.kompex.backend.service.OsobaService;
import com.kompex.backend.service.UzytkownikService;
import com.kompex.backend.service.ZamowienieService;
import com.kompex.backend.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * @author grzechu
 */
@RestController
@RequestMapping("/api")
public class ZamowienieController {

    public static final Logger logger = LoggerFactory.getLogger(ZamowienieController.class);

    @Autowired
    ZamowienieService zamowienieService;

    @Autowired
    UzytkownikService uzytkownikService;

    @Autowired
    OsobaService osobaService;

    @RequestMapping(value = "/zamowienies/", method = RequestMethod.GET)
    public ResponseEntity<List<Zamowienie>> listAllZamowienies() {
        List<Zamowienie> zamowienies = zamowienieService.findAllZamowienies();
        zamowienies.forEach(zamowienie -> {
            zamowienie.getUzytkownik().setPassword(null);
            zamowienie.getUzytkownik().setOsoba(uzytkownikService.findById(zamowienie.getUzytkownik().getIdUzytkownik()).getOsoba());
        });
        if (zamowienies.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(zamowienies, HttpStatus.OK);
    }

    @RequestMapping(value = "/zamowienie/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getZamowienie(@PathVariable("id") long id) {
        logger.info("Fetching Zamowienie with id {}", id);
        Zamowienie zamowienie = zamowienieService.findById(id);
        if (zamowienie == null) {
            logger.error("Zamowienie with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Zamowienie with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Zamowienie>(zamowienie, HttpStatus.OK);
    }

    @RequestMapping(value = "/zamowienie/", method = RequestMethod.POST)
    public ResponseEntity<?> createZamowienie(@RequestBody Zamowienie zamowienie, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Zamowienie : {}", zamowienie);
        Osoba nowy;
        if (zamowienie.getUzytkownik() != null && zamowienie.getUzytkownik().getOsoba() != null) {
            nowy = osobaService.findByNazwisko(zamowienie.getUzytkownik().getOsoba().getNazwisko());
            if (nowy == null) {
                osobaService.saveOsoba(new Osoba(zamowienie.getUzytkownik().getOsoba().getImie(), zamowienie.getUzytkownik().getOsoba().getNazwisko()));
            }
            zamowienie.getUzytkownik().setOsoba(nowy);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        Uzytkownik uzyt = uzytkownikService.findByUsername(zamowienie.getUzytkownik().getUsername());
        if (uzyt != null) {
            if (uzyt.getOsoba() == null) {
                uzyt.setOsoba(nowy);
                uzytkownikService.updateUzytkownik(uzyt);
            }
            zamowienie.setUzytkownik(uzyt);
            zamowienieService.saveZamowienie(zamowienie);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/zamowienie/{id}").buildAndExpand(zamowienie.getIdZamowienie()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/zamowienie/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateZamowienie(@PathVariable("id") long id, @RequestBody Zamowienie zamowienie) {
        logger.info("Updating Zamowienie with id {}", id);

        Zamowienie currentZamowienie = zamowienieService.findById(id);

        if (currentZamowienie == null) {
            logger.error("Unable to update. Zamowienie with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. Zamowienie with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        zamowienieService.updateZamowienie(zamowienie);
        return new ResponseEntity<Zamowienie>(zamowienie, HttpStatus.OK);
    }

    @RequestMapping(value = "/zamowienie/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteZamowienie(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting Zamowienie with id {}", id);

        Zamowienie zamowienie = zamowienieService.findById(id);
        if (zamowienie == null) {
            logger.error("Unable to delete. Zamowienie with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to delete. Zamowienie with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        zamowienieService.deleteZamowienieById(id);
        return new ResponseEntity<Zamowienie>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/zamowienie/", method = RequestMethod.DELETE)
    public ResponseEntity<Zamowienie> deleteAllZamowienies() {
        logger.info("Deleting All Zamowienies");

        zamowienieService.deleteAllZamowienies();
        return new ResponseEntity<Zamowienie>(HttpStatus.NO_CONTENT);
    }

}
