package com.kompex.backend.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author grzechu
 */
@RestController
public class AppController {

    @RequestMapping("/")
    public String home() {
        return "index";
    }
}
