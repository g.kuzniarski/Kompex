package com.kompex.backend.controller;

import com.kompex.backend.model.Uzytkownik;
import com.kompex.backend.model.dicRola;
import com.kompex.backend.service.UzytkownikService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/api")
public class LoginController {

    public static final Logger logger = LoggerFactory.getLogger(UzytkownikController.class);

    @Autowired
    UzytkownikService uzytkownikService;

    private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> login(@RequestBody Uzytkownik uzytkownik, UriComponentsBuilder ucBuilder) {
        Uzytkownik user = uzytkownikService.findByUsername(uzytkownik.getUsername());
        if (user != null && user.isEnabled()
                && encoder.matches(uzytkownik.getPassword(), user.getPassword())) {
            LoginReturn loginReturn = new LoginReturn();
            loginReturn.token = encoder.encode("TOKEN");
            loginReturn.userRole = user.getRola().toString();
            return new ResponseEntity<LoginReturn>(loginReturn, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<?> register(@RequestBody Uzytkownik uzytkownik, UriComponentsBuilder ucBuilder) {
        Uzytkownik user = uzytkownikService.findByUsername(uzytkownik.getUsername());
        if (user == null) {
            uzytkownik.setPassword(encoder.encode(uzytkownik.getPassword()));
            uzytkownik.setEnabled(true);
            uzytkownik.setRola(dicRola.ROLA_USER);
            uzytkownikService.saveUzytkownik(uzytkownik);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    public class LoginReturn {
        public String token;
        public String userRole;

        public LoginReturn() {
        }
    }
}
