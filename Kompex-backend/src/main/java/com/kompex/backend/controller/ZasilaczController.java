package com.kompex.backend.controller;

import com.kompex.backend.model.Zasilacz;
import com.kompex.backend.service.ZasilaczService;
import com.kompex.backend.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * @author grzechu
 */
@RestController
@RequestMapping("/api")
public class ZasilaczController {

    public static final Logger logger = LoggerFactory.getLogger(ZasilaczController.class);

    @Autowired
    ZasilaczService zasilaczService;

    @RequestMapping(value = "/zasilaczs/", method = RequestMethod.GET)
    public ResponseEntity<List<Zasilacz>> listAllZasilaczs() {
        List<Zasilacz> zasilaczs = zasilaczService.findAllZasilaczs();
        if (zasilaczs.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(zasilaczs, HttpStatus.OK);
    }

    @RequestMapping(value = "/zasilacz/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getZasilacz(@PathVariable("id") long id) {
        logger.info("Fetching Zasilacz with id {}", id);
        Zasilacz zasilacz = zasilaczService.findById(id);
        if (zasilacz == null) {
            logger.error("Zasilacz with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Zasilacz with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Zasilacz>(zasilacz, HttpStatus.OK);
    }

    @RequestMapping(value = "/zasilacz/", method = RequestMethod.POST)
    public ResponseEntity<?> createZasilacz(@RequestBody Zasilacz zasilacz, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Zasilacz : {}", zasilacz);

        if (zasilaczService.isZasilaczExist(zasilacz)) {
            logger.error("Unable to create. A Zasilacz with name {} already exist", zasilacz.getNazwa());
            return new ResponseEntity<>(new CustomErrorType("Unable to create. A Zasilacz with name "
                    + zasilacz.getNazwa() + " already exist."), HttpStatus.CONFLICT);
        }
        zasilaczService.saveZasilacz(zasilacz);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/zasilacz/{id}").buildAndExpand(zasilacz.getIdZasilacz()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/zasilacz/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateZasilacz(@PathVariable("id") long id, @RequestBody Zasilacz zasilacz) {
        logger.info("Updating Zasilacz with id {}", id);

        Zasilacz currentZasilacz = zasilaczService.findById(id);

        if (currentZasilacz == null) {
            logger.error("Unable to update. Zasilacz with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. Zasilacz with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        zasilaczService.updateZasilacz(zasilacz);
        return new ResponseEntity<Zasilacz>(currentZasilacz, HttpStatus.OK);
    }

    @RequestMapping(value = "/zasilacz/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteZasilacz(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting Zasilacz with id {}", id);

        Zasilacz zasilacz = zasilaczService.findById(id);
        if (zasilacz == null) {
            logger.error("Unable to delete. Zasilacz with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to delete. Zasilacz with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        zasilaczService.deleteZasilaczById(id);
        return new ResponseEntity<Zasilacz>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/zasilacz/", method = RequestMethod.DELETE)
    public ResponseEntity<Zasilacz> deleteAllZasilaczs() {
        logger.info("Deleting All Zasilaczs");

        zasilaczService.deleteAllZasilaczs();
        return new ResponseEntity<Zasilacz>(HttpStatus.NO_CONTENT);
    }

}
