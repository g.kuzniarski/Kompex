package com.kompex.backend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

@RestController
@RequestMapping("/api")
public class ImagesController {

    public static final Logger logger = LoggerFactory.getLogger(ImagesController.class);

    @RequestMapping(
            value = "/image/{f}",
            method = RequestMethod.GET
    )
    public ResponseEntity<String> getImage(@PathVariable("f") String fileName) throws IOException {

        String path = "/static/" + fileName + ".jpg";
        InputStream in = getClass().getResourceAsStream(path);

        String encodeImage = null;

        try {
            byte[] targetArray = new byte[in.available()];

            in.read(targetArray);
            encodeImage = Base64.getEncoder().withoutPadding().encodeToString(targetArray);
        } catch (NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        if (encodeImage != null && !encodeImage.equals("")) {
            return new ResponseEntity<>(encodeImage, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
