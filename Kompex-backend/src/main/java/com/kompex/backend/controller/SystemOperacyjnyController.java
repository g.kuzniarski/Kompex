package com.kompex.backend.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.kompex.backend.model.SystemOperacyjny;
import com.kompex.backend.service.SystemOperacyjnyService;
import com.kompex.backend.util.CustomErrorType;

/**
 * @author grzechu
 */
@RestController
@RequestMapping("/api")
public class SystemOperacyjnyController {

    public static final Logger logger = LoggerFactory.getLogger(SystemOperacyjnyController.class);

    @Autowired
    SystemOperacyjnyService systemOperacyjnyService;

    @RequestMapping(value = "/systemOperacyjnys/", method = RequestMethod.GET)
    public ResponseEntity<List<SystemOperacyjny>> listAllSystemOperacyjnys() {
        List<SystemOperacyjny> systemOperacyjnys = systemOperacyjnyService.findAllSystemOperacyjnys();
        if (systemOperacyjnys.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(systemOperacyjnys, HttpStatus.OK);
    }

    @RequestMapping(value = "/systemOperacyjny/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getSystemOperacyjny(@PathVariable("id") long id) {
        logger.info("Fetching SystemOperacyjny with id {}", id);
        SystemOperacyjny systemOperacyjny = systemOperacyjnyService.findById(id);
        if (systemOperacyjny == null) {
            logger.error("SystemOperacyjny with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("SystemOperacyjny with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<SystemOperacyjny>(systemOperacyjny, HttpStatus.OK);
    }

    @RequestMapping(value = "/systemOperacyjny/", method = RequestMethod.POST)
    public ResponseEntity<?> createSystemOperacyjny(@RequestBody SystemOperacyjny systemOperacyjny, UriComponentsBuilder ucBuilder) {
        logger.info("Creating SystemOperacyjny : {}", systemOperacyjny);

        if (systemOperacyjnyService.isSystemOperacyjnyExist(systemOperacyjny)) {
            logger.error("Unable to create. A SystemOperacyjny with name {} already exist", systemOperacyjny.getNazwa());
            return new ResponseEntity<>(new CustomErrorType("Unable to create. A SystemOperacyjny with name "
                    + systemOperacyjny.getNazwa() + " already exist."), HttpStatus.CONFLICT);
        }
        systemOperacyjnyService.saveSystemOperacyjny(systemOperacyjny);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/systemOperacyjny/{id}").buildAndExpand(systemOperacyjny.getIdSystemOperacyjny()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/systemOperacyjny/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateSystemOperacyjny(@PathVariable("id") long id, @RequestBody SystemOperacyjny systemOperacyjny) {
        logger.info("Updating SystemOperacyjny with id {}", id);

        SystemOperacyjny currentSystemOperacyjny = systemOperacyjnyService.findById(id);

        if (currentSystemOperacyjny == null) {
            logger.error("Unable to update. SystemOperacyjny with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. SystemOperacyjny with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        systemOperacyjny.setIdSystemOperacyjny(id);
        systemOperacyjnyService.updateSystemOperacyjny(systemOperacyjny);
        return new ResponseEntity<SystemOperacyjny>(systemOperacyjny, HttpStatus.OK);
    }

    @RequestMapping(value = "/systemOperacyjny/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteSystemOperacyjny(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting SystemOperacyjny with id {}", id);

        SystemOperacyjny systemOperacyjny = systemOperacyjnyService.findById(id);
        if (systemOperacyjny == null) {
            logger.error("Unable to delete. SystemOperacyjny with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to delete. SystemOperacyjny with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        systemOperacyjnyService.deleteSystemOperacyjnyById(id);
        return new ResponseEntity<SystemOperacyjny>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/systemOperacyjny/", method = RequestMethod.DELETE)
    public ResponseEntity<SystemOperacyjny> deleteAllSystemOperacyjnys() {
        logger.info("Deleting All SystemOperacyjnys");

        systemOperacyjnyService.deleteAllSystemOperacyjnys();
        return new ResponseEntity<SystemOperacyjny>(HttpStatus.NO_CONTENT);
    }

}
