package com.kompex.backend.controller;

import com.kompex.backend.model.RAM;
import com.kompex.backend.service.RAMService;
import com.kompex.backend.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * @author grzechu
 */
@RestController
@RequestMapping("/api")
public class RAMController {

    public static final Logger logger = LoggerFactory.getLogger(RAMController.class);

    @Autowired
    RAMService rAMService;

    @RequestMapping(value = "/rams/", method = RequestMethod.GET)
    public ResponseEntity<List<RAM>> listAllRAMs() {
        List<RAM> rAMs = rAMService.findAllRAMs();
        if (rAMs.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(rAMs, HttpStatus.OK);
    }

    @RequestMapping(value = "/ram/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getRAM(@PathVariable("id") long id) {
        logger.info("Fetching RAM with id {}", id);
        RAM rAM = rAMService.findById(id);
        if (rAM == null) {
            logger.error("RAM with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("RAM with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<RAM>(rAM, HttpStatus.OK);
    }

    @RequestMapping(value = "/ram/", method = RequestMethod.POST)
    public ResponseEntity<?> createRAM(@RequestBody RAM rAM, UriComponentsBuilder ucBuilder) {
        logger.info("Creating RAM : {}", rAM);

        if (rAMService.isRAMExist(rAM)) {
            logger.error("Unable to create. A RAM with name {} already exist", rAM.getNazwa());
            return new ResponseEntity<>(new CustomErrorType("Unable to create. A RAM with name "
                    + rAM.getNazwa() + " already exist."), HttpStatus.CONFLICT);
        }
        rAMService.saveRAM(rAM);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/ram/{id}").buildAndExpand(rAM.getIdRAM()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/ram/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateRAM(@PathVariable("id") long id, @RequestBody RAM rAM) {
        logger.info("Updating RAM with id {}", id);

        RAM currentRAM = rAMService.findById(id);

        if (currentRAM == null) {
            logger.error("Unable to update. RAM with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. RAM with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        rAMService.updateRAM(rAM);
        return new ResponseEntity<RAM>(rAM, HttpStatus.OK);
    }

    @RequestMapping(value = "/ram/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteRAM(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting RAM with id {}", id);

        RAM rAM = rAMService.findById(id);
        if (rAM == null) {
            logger.error("Unable to delete. RAM with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to delete. RAM with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        rAMService.deleteRAMById(id);
        return new ResponseEntity<RAM>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/ram/", method = RequestMethod.DELETE)
    public ResponseEntity<RAM> deleteAllRAMs() {
        logger.info("Deleting All RAMs");

        rAMService.deleteAllRAMs();
        return new ResponseEntity<RAM>(HttpStatus.NO_CONTENT);
    }

}
