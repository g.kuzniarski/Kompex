package com.kompex.backend.controller;

import com.kompex.backend.model.Komputer;
import com.kompex.backend.service.KomputerService;
import com.kompex.backend.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * @author grzechu
 */
@RestController
@RequestMapping("/api")
public class KomputerController {

    public static final Logger logger = LoggerFactory.getLogger(KomputerController.class);

    @Autowired
    KomputerService komputerService;

    @RequestMapping(value = "/komputers/", method = RequestMethod.GET)
    public ResponseEntity<List<Komputer>> listAllKomputers() {
        List<Komputer> komputers = komputerService.findAllKomputers();
        if (komputers.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(komputers, HttpStatus.OK);
    }

    @RequestMapping(value = "/komputer/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getKomputer(@PathVariable("id") long id) {
        logger.info("Fetching Komputer with id {}", id);
        Komputer komputer = komputerService.findById(id);
        if (komputer == null) {
            logger.error("Komputer with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Komputer with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Komputer>(komputer, HttpStatus.OK);
    }

    @RequestMapping(value = "/komputer/", method = RequestMethod.POST)
    public ResponseEntity<?> createKomputer(@RequestBody Komputer komputer, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Komputer : {}", komputer);

        komputerService.saveKomputer(komputer);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/komputer/{id}").buildAndExpand(komputer.getIdKomputer()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/komputer/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateKomputer(@PathVariable("id") long id, @RequestBody Komputer komputer) {
        logger.info("Updating Komputer with id {}", id);

        Komputer currentKomputer = komputerService.findById(id);

        if (currentKomputer == null) {
            logger.error("Unable to update. Komputer with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. Komputer with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        komputerService.updateKomputer(komputer);
        return new ResponseEntity<Komputer>(komputer, HttpStatus.OK);
    }

    @RequestMapping(value = "/komputer/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteKomputer(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting Komputer with id {}", id);

        Komputer komputer = komputerService.findById(id);
        if (komputer == null) {
            logger.error("Unable to delete. Komputer with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to delete. Komputer with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        komputerService.deleteKomputerById(id);
        return new ResponseEntity<Komputer>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/komputer/", method = RequestMethod.DELETE)
    public ResponseEntity<Komputer> deleteAllKomputers() {
        logger.info("Deleting All Komputers");

        komputerService.deleteAllKomputers();
        return new ResponseEntity<Komputer>(HttpStatus.NO_CONTENT);
    }

}
