package com.kompex.backend.controller;

import com.kompex.backend.model.Dysk;
import com.kompex.backend.service.DyskService;
import com.kompex.backend.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * @author grzechu
 */
@RestController
@RequestMapping("/api")
public class DyskController {

    public static final Logger logger = LoggerFactory.getLogger(DyskController.class);

    @Autowired
    DyskService dyskService;

    @RequestMapping(value = "/dysks/", method = RequestMethod.GET)
    public ResponseEntity<List<Dysk>> listAllDysks() {
        List<Dysk> dysks = dyskService.findAllDysks();
        if (dysks.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(dysks, HttpStatus.OK);
    }

    @RequestMapping(value = "/dysk/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getDysk(@PathVariable("id") long id) {
        logger.info("Fetching Dysk with id {}", id);
        Dysk dysk = dyskService.findById(id);
        if (dysk == null) {
            logger.error("Dysk with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Dysk with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Dysk>(dysk, HttpStatus.OK);
    }

    @RequestMapping(value = "/dysk/", method = RequestMethod.POST)
    public ResponseEntity<?> createDysk(@RequestBody Dysk dysk, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Dysk : {}", dysk);

        if (dyskService.isDyskExist(dysk)) {
            logger.error("Unable to create. A Dysk with name {} already exist", dysk.getNazwa());
            return new ResponseEntity<>(new CustomErrorType("Unable to create. A Dysk with name "
                    + dysk.getNazwa() + " already exist."), HttpStatus.CONFLICT);
        }
        dyskService.saveDysk(dysk);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/dysk/{id}").buildAndExpand(dysk.getIdDysk()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/dysk/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateDysk(@PathVariable("id") long id, @RequestBody Dysk dysk) {
        logger.info("Updating Dysk with id {}", id);

        Dysk currentDysk = dyskService.findById(id);

        if (currentDysk == null) {
            logger.error("Unable to update. Dysk with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. Dysk with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        dyskService.updateDysk(dysk);
        return new ResponseEntity<Dysk>(dysk, HttpStatus.OK);
    }

    @RequestMapping(value = "/dysk/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteDysk(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting Dysk with id {}", id);

        Dysk dysk = dyskService.findById(id);
        if (dysk == null) {
            logger.error("Unable to delete. Dysk with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to delete. Dysk with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        dyskService.deleteDyskById(id);
        return new ResponseEntity<Dysk>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/dysk/", method = RequestMethod.DELETE)
    public ResponseEntity<Dysk> deleteAllDysks() {
        logger.info("Deleting All Dysks");

        dyskService.deleteAllDysks();
        return new ResponseEntity<Dysk>(HttpStatus.NO_CONTENT);
    }

}
