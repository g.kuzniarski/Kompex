package com.kompex.backend.controller;

import com.kompex.backend.model.Osoba;
import com.kompex.backend.service.OsobaService;
import com.kompex.backend.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * @author grzechu
 */
@RestController
@RequestMapping("/api")
public class OsobaController {

    public static final Logger logger = LoggerFactory.getLogger(OsobaController.class);

    @Autowired
    OsobaService osobaService;

    @RequestMapping(value = "/osobas/", method = RequestMethod.GET)
    public ResponseEntity<List<Osoba>> listAllOsobas() {
        List<Osoba> osobas = osobaService.findAllOsobas();
        if (osobas.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(osobas, HttpStatus.OK);
    }

    @RequestMapping(value = "/osoba/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getOsoba(@PathVariable("id") long id) {
        logger.info("Fetching Osoba with id {}", id);
        Osoba osoba = osobaService.findById(id);
        if (osoba == null) {
            logger.error("Osoba with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Osoba with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Osoba>(osoba, HttpStatus.OK);
    }

    @RequestMapping(value = "/osoba/", method = RequestMethod.POST)
    public ResponseEntity<?> createOsoba(@RequestBody Osoba osoba, UriComponentsBuilder ucBuilder) {
        logger.info("Creating Osoba : {}", osoba);

        osobaService.saveOsoba(osoba);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/osoba/{id}").buildAndExpand(osoba.getIdOsoba()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/osoba/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateOsoba(@PathVariable("id") long id, @RequestBody Osoba osoba) {
        logger.info("Updating Osoba with id {}", id);

        Osoba currentOsoba = osobaService.findById(id);

        if (currentOsoba == null) {
            logger.error("Unable to update. Osoba with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. Osoba with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        osobaService.updateOsoba(currentOsoba);
        return new ResponseEntity<Osoba>(osoba, HttpStatus.OK);
    }

    @RequestMapping(value = "/osoba/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteOsoba(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting Osoba with id {}", id);

        Osoba osoba = osobaService.findById(id);
        if (osoba == null) {
            logger.error("Unable to delete. Osoba with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to delete. Osoba with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        osobaService.deleteOsobaById(id);
        return new ResponseEntity<Osoba>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/osoba/", method = RequestMethod.DELETE)
    public ResponseEntity<Osoba> deleteAllOsobas() {
        logger.info("Deleting All Osobas");

        osobaService.deleteAllOsobas();
        return new ResponseEntity<Osoba>(HttpStatus.NO_CONTENT);
    }

}
