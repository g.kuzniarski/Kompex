package com.kompex.backend.controller;

import com.kompex.backend.model.CPU;
import com.kompex.backend.service.CPUService;
import com.kompex.backend.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author grzechu
 */
@RestController
@RequestMapping("/api")
public class CPUController {

    public static final Logger logger = LoggerFactory.getLogger(CPUController.class);

    @Autowired
    CPUService cPUService;

    @RequestMapping(value = "/cpus/", method = RequestMethod.GET)
    public ResponseEntity<List<CPU>> listAllCPUs() {
        List<CPU> cPUs = cPUService.findAllCPUs();
        if (cPUs.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(cPUs, HttpStatus.OK);
    }

    @RequestMapping(value = "/cpu/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getCPU(@PathVariable("id") long id) {
        logger.info("Fetching CPU with id {}", id);
        CPU cPU = cPUService.findById(id);
        if (cPU == null) {
            logger.error("CPU with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("CPU with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<CPU>(cPU, HttpStatus.OK);
    }

    @RequestMapping(value = "/cpu/", method = RequestMethod.POST)
    public ResponseEntity<?> createCPU(@RequestBody CPU cPU, UriComponentsBuilder ucBuilder) {
        logger.info("Creating CPU : {}", cPU);

        if (cPUService.isCPUExist(cPU)) {
            logger.error("Unable to create. A CPU with name {} already exist", cPU.getNazwa());
            return new ResponseEntity<>(new CustomErrorType("Unable to create. A CPU with name "
                    + cPU.getNazwa() + " already exist."), HttpStatus.CONFLICT);
        }
        cPUService.saveCPU(cPU);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/cpu/{id}").buildAndExpand(cPU.getIdCPU()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/cpu/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateCPU(@PathVariable("id") long id, @RequestBody CPU cPU) {
        logger.info("Updating CPU with id {}", id);

        CPU currentCPU = cPUService.findById(id);

        if (currentCPU == null) {
            logger.error("Unable to update. CPU with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. CPU with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        cPUService.updateCPU(cPU);
        return new ResponseEntity<CPU>(cPU, HttpStatus.OK);
    }

    @RequestMapping(value = "/cpu/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteCPU(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting CPU with id {}", id);

        CPU cPU = cPUService.findById(id);
        if (cPU == null) {
            logger.error("Unable to delete. CPU with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to delete. CPU with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        cPUService.deleteCPUById(id);
        return new ResponseEntity<CPU>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/cpu/", method = RequestMethod.DELETE)
    public ResponseEntity<CPU> deleteAllCPUs() {
        logger.info("Deleting All CPUs");

        cPUService.deleteAllCPUs();
        return new ResponseEntity<CPU>(HttpStatus.NO_CONTENT);
    }

}
