package com.kompex.backend.controller;

import com.kompex.backend.model.GPU;
import com.kompex.backend.service.GPUService;
import com.kompex.backend.util.CustomErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * @author grzechu
 */
@RestController
@RequestMapping("/api")
public class GPUController {

    public static final Logger logger = LoggerFactory.getLogger(GPUController.class);

    @Autowired
    GPUService gPUService;

    @RequestMapping(value = "/gpus/", method = RequestMethod.GET)
    public ResponseEntity<List<GPU>> listAllGPUs() {
        List<GPU> gPUs = gPUService.findAllGPUs();
        if (gPUs.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(gPUs, HttpStatus.OK);
    }

    @RequestMapping(value = "/gpu/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getGPU(@PathVariable("id") long id) {
        logger.info("Fetching GPU with id {}", id);
        GPU gPU = gPUService.findById(id);
        if (gPU == null) {
            logger.error("GPU with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("GPU with id " + id
                    + " not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<GPU>(gPU, HttpStatus.OK);
    }

    @RequestMapping(value = "/gpu/", method = RequestMethod.POST)
    public ResponseEntity<?> createGPU(@RequestBody GPU gPU, UriComponentsBuilder ucBuilder) {
        logger.info("Creating GPU : {}", gPU);

        if (gPUService.isGPUExist(gPU)) {
            logger.error("Unable to create. A GPU with name {} already exist", gPU.getNazwa());
            return new ResponseEntity<>(new CustomErrorType("Unable to create. A GPU with name "
                    + gPU.getNazwa() + " already exist."), HttpStatus.CONFLICT);
        }
        gPUService.saveGPU(gPU);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/api/gpu/{id}").buildAndExpand(gPU.getIdGPU()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/gpu/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateGPU(@PathVariable("id") long id, @RequestBody GPU gPU) {
        logger.info("Updating GPU with id {}", id);

        GPU currentGPU = gPUService.findById(id);

        if (currentGPU == null) {
            logger.error("Unable to update. GPU with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to upate. GPU with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }

        gPUService.updateGPU(gPU);
        return new ResponseEntity<GPU>(gPU, HttpStatus.OK);
    }

    @RequestMapping(value = "/gpu/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteGPU(@PathVariable("id") long id) {
        logger.info("Fetching & Deleting GPU with id {}", id);

        GPU gPU = gPUService.findById(id);
        if (gPU == null) {
            logger.error("Unable to delete. GPU with id {} not found.", id);
            return new ResponseEntity<>(new CustomErrorType("Unable to delete. GPU with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
        gPUService.deleteGPUById(id);
        return new ResponseEntity<GPU>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/gpu/", method = RequestMethod.DELETE)
    public ResponseEntity<GPU> deleteAllGPUs() {
        logger.info("Deleting All GPUs");

        gPUService.deleteAllGPUs();
        return new ResponseEntity<GPU>(HttpStatus.NO_CONTENT);
    }

}
