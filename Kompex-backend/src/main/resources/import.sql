USE Kompex;

#Początkowe wartości importowane przy budowaniu bazy
#Admin
INSERT INTO Osoba (Imie, Nazwisko) VALUES ('Grzegorz', 'Kuzniarski');
INSERT INTO Uzytkownik (Username, Password, Enabled, idOsoba, Rola) VALUES ('admin', '$2a$10$IF/yWtUX7x5r88T6NiE4TeJ9xJ35/pIERP5SCi9EyZi8RAa/Swaee', true, (SELECT 1 idOsoba FROM Osoba o WHERE o.Imie = 'Grzegorz' AND o.Nazwisko = 'Kuzniarski'), 'ROLA_ADMIN');

#Tabele
#Systemy operacyjne
INSERT INTO SystemOperacyjny (Nazwa, Producent, Cena, ImageName) VALUES ('Windows 10 Pro', 'Microsoft', 1100, 'w10pro');
INSERT INTO SystemOperacyjny (Nazwa, Producent, Cena, ImageName) VALUES ('Windows 10 Home', 'Microsoft', 630, 'w10home');
INSERT INTO SystemOperacyjny (Nazwa, Producent, Cena, ImageName) VALUES ('Ubuntu 16.04', 'Canonical', 100, 'ubuntu');
INSERT INTO SystemOperacyjny (Nazwa, Producent, Cena, ImageName) VALUES ('Brak', '', 0, 'empty');

#CPU
INSERT INTO CPU (Nazwa, Producent, Cena, Czetotliwosc_taktowania, Ilosc_rdzeni, Pojemnosc_pamieci_cache_L2, Typ_gniazda, ImageName) VALUES ('Ryzen 5 1600', 'AMD', 865, 3200, 6, 3, 'Socket AM4', 'ryzen5');
INSERT INTO CPU (Nazwa, Producent, Cena, Czetotliwosc_taktowania, Ilosc_rdzeni, Pojemnosc_pamieci_cache_L2, Typ_gniazda, ImageName) VALUES ('Core i5-7500', 'Intel', 820, 3400, 4, 1, 'Socket 1151', 'inteli5');
INSERT INTO CPU (Nazwa, Producent, Cena, Czetotliwosc_taktowania, Ilosc_rdzeni, Pojemnosc_pamieci_cache_L2, Typ_gniazda, ImageName) VALUES ('Core i7-7700K', 'Intel', 1400, 4200, 4, 1, 'Socket 1151', 'inteli7');
INSERT INTO CPU (Nazwa, Producent, Cena, Czetotliwosc_taktowania, Ilosc_rdzeni, Pojemnosc_pamieci_cache_L2, Typ_gniazda, ImageName) VALUES ('Ryzen 3 1200', 'AMD', 450, 3100, 4, 2, 'Socket AM4', 'ryzen3');

#Dyski
INSERT INTO Dysk (Nazwa, Producent, Cena, Typ, Interfejs, Pojemnosc, ImageName) VALUES ('P300', 'Toshiba', 180, 'HDD', 'SATA III', 1024, 'toshiba1');
INSERT INTO Dysk (Nazwa, Producent, Cena, Typ, Interfejs, Pojemnosc, ImageName) VALUES ('BarraCuda', 'Seagate', 190, 'HDD', 'SATA III', 1024, 'seagate1');
INSERT INTO Dysk (Nazwa, Producent, Cena, Typ, Interfejs, Pojemnosc, ImageName) VALUES ('850 EVO', 'Samsung', 400, 'SSD', 'SATA III', 250, 'samsung250');
INSERT INTO Dysk (Nazwa, Producent, Cena, Typ, Interfejs, Pojemnosc, ImageName) VALUES ('SU800', 'ADATA', 235, 'SSD', 'SATA III', 128, 'adata128');

#GPU
INSERT INTO GPU (Nazwa, Producent, Cena, Pamiec, Taktowanie, Typ_pamieci, Typ_zlacza, ImageName) VALUES ('FirePro W4100', 'AMD', 775, 2, 2250, 'GDDR5', 'PCI_Express_x16', 'W4100');
INSERT INTO GPU (Nazwa, Producent, Cena, Pamiec, Taktowanie, Typ_pamieci, Typ_zlacza, ImageName) VALUES ('GeForce GTX 1050 Ti', 'NVIDIA', 840, 4, 7000, 'GDDR5', 'PCI_Express_x16', 'gtx1050');
INSERT INTO GPU (Nazwa, Producent, Cena, Pamiec, Taktowanie, Typ_pamieci, Typ_zlacza, ImageName) VALUES ('GeForce GTX 1060', 'NVIDIA', 1800, 6, 8000, 'GDDR5', 'PCI_Express_x16', 'gtx1060');
INSERT INTO GPU (Nazwa, Producent, Cena, Pamiec, Taktowanie, Typ_pamieci, Typ_zlacza, ImageName) VALUES ('FirePro W5100', 'AMD', 1550, 4, 3000, 'GDDR5', 'PCI_Express_x16', 'W5100');

#RAM
INSERT INTO RAM (Nazwa, Producent, Cena, Czestotliwosc, Pojemnosc, Typ_pamieci, ImageName) VALUES ('Fury', 'HyperX', 370, 2133, 8, 'DDR4', 'hyperx');
INSERT INTO RAM (Nazwa, Producent, Cena, Czestotliwosc, Pojemnosc, Typ_pamieci, ImageName) VALUES ('Sport LT', 'Ballistix', 1585, 2666, 32, 'DDR4', 'ballistix');
INSERT INTO RAM (Nazwa, Producent, Cena, Czestotliwosc, Pojemnosc, Typ_pamieci, ImageName) VALUES ('Vengeance LPX', 'Corsair', 1010, 3000, 16, 'DDR4', 'corsair');
INSERT INTO RAM (Nazwa, Producent, Cena, Czestotliwosc, Pojemnosc, Typ_pamieci, ImageName) VALUES ('IRDM', 'GoodRam', 360, 2400, 8, 'DDR4', 'goodram');

#Zasilacze
INSERT INTO Zasilacz (Nazwa, Producent, Cena, Moc, ImageName ) VALUES ('Volcano', 'Modecom', 275, 750, 'volcano');
INSERT INTO Zasilacz (Nazwa, Producent, Cena, Moc, ImageName ) VALUES ('Vero', 'SilentiumPC', 230, 600, 'vero');
INSERT INTO Zasilacz (Nazwa, Producent, Cena, Moc, ImageName ) VALUES ('System Power 8', 'be quiet!', 180, 400, 'bequiet');
INSERT INTO Zasilacz (Nazwa, Producent, Cena, Moc, ImageName ) VALUES ('VS', 'Corsair', 550, 185, 'vs550');