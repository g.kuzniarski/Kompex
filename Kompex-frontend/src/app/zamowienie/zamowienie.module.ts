import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ZamowienieComponent} from './zamowienie.component';
import {ZamowienieService} from '../services/zamowienie.service';
import {SystemOperacyjnyModule} from '../system-operacyjny/system-operacyjny.module';
import {DyskModule} from '../dysk/dysk.module';
import {StepsModule} from 'primeng/primeng';
import {CPUModule} from '../cpu/cpu.module';
import {GPUModule} from '../gpu/gpu.module';
import {RAMModule} from '../ram/ram.module';
import {UzytkownikModule} from '../uzytkownik/uzytkownik.module';
import {ZasilaczModule} from '../zasilacz/zasilacz.module';
import {PodsumowanieModule} from '../podsumowanie/podsumowanie.module';
import {LoginGuard} from '../login/login.guard';

@NgModule({
  imports: [
    CommonModule,
    StepsModule,
    SystemOperacyjnyModule,
    DyskModule,
    CPUModule,
    GPUModule,
    RAMModule,
    UzytkownikModule,
    ZasilaczModule,
    PodsumowanieModule
  ],
  declarations: [ZamowienieComponent],
  providers: [LoginGuard, ZamowienieService],
  exports: [ZamowienieComponent]
})
export class ZamowienieModule {
}
