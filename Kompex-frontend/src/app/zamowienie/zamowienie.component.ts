import {Component, OnInit} from '@angular/core';
import {MenuItem} from 'primeng/primeng';
import {Zamowienie} from '../DBDatatypes';
import {ActivatedRoute, Router} from '@angular/router';
import {BASE_ROUTE} from '../app-routing.module';

@Component({
  selector: 'app-zamowienie',
  templateUrl: './zamowienie.component.html',
  styleUrls: ['./zamowienie.component.scss']
})
export class ZamowienieComponent implements OnInit {

  private activeIndex = 0;
  private items: MenuItem[] = [];
  private zamowienie: Zamowienie;

  constructor(private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    BASE_ROUTE.zamowienie.items.forEach(value => {
      this.items.push({
        label: value.label,
        routerLink: value.routerLink.substring('zamowienie/'.length)
      });
    });

    if (!this.zamowienie) {
      this.zamowienie = this.createEmptyZamowienie();
    }

    this.activeIndex = this.items.findIndex(val => this.router.url.endsWith(val.routerLink));
  }

  private createEmptyZamowienie(): Zamowienie {
    return {
      komputer: null,
      uzytkownik: null,
      data: null
    };
  }

  private onStepClicked() {
  }
}
