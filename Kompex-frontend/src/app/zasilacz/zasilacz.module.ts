import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ZasilaczComponent} from './zasilacz.component';
import {ZasilaczService} from '../services/zasilacz.service';
import {DataListModule, ButtonModule, GrowlModule} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    DataListModule,
    ButtonModule,
    GrowlModule
  ],
  declarations: [ZasilaczComponent],
  providers: [ZasilaczService],
  exports: [ZasilaczComponent]
})
export class ZasilaczModule {
}
