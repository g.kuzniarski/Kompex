import {Component, OnInit} from '@angular/core';
import {ZasilaczService} from '../services/zasilacz.service';
import {Zasilacz} from '../DBDatatypes';
import {ImageService} from '../services/image.service';
import {DomSanitizer} from '@angular/platform-browser';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-zasilacz',
  templateUrl: './zasilacz.component.html',
  styleUrls: ['./zasilacz.component.scss']
})
export class ZasilaczComponent implements OnInit {

  msgs: Message[] = [];
  private zasilaczs: Zasilacz[];

  constructor(private zasilaczService: ZasilaczService, private imageService: ImageService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.zasilaczService.getZasilaczLista().subscribe(zasilaczs => {
      this.zasilaczs = zasilaczs;
      this.zasilaczs.forEach(zasilacz => {
        this.imageService.getImage(zasilacz.imageName).subscribe(img =>
          zasilacz.image = this.sanitizer.bypassSecurityTrustUrl('data:image/JPEG;base64,' + img.body));
      });
    });
  }

  onAddPrimaryClick(zasilacz: Zasilacz) {
    sessionStorage.setItem('zamowienie.komputer.zasilacz.id', zasilacz.idZasilacz.toString());
    this.msgs = [];
    this.msgs.push({
      severity: 'success',
      summary: 'Aktualizacja koszyka',
      detail: 'Dodano zasilacz ' + zasilacz.producent + ' ' + zasilacz.nazwa
    });
  }
}
