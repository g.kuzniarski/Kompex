import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrzegladComponent } from './przeglad.component';

describe('PrzegladComponent', () => {
  let component: PrzegladComponent;
  let fixture: ComponentFixture<PrzegladComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrzegladComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrzegladComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
