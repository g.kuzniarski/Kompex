import {Component, OnInit} from '@angular/core';
import {Komputer, Osoba, Zamowienie} from '../DBDatatypes';
import {ZamowienieService} from '../services/zamowienie.service';
import {LoginService} from '../services/login.service';

@Component({
  selector: 'app-przeglad',
  templateUrl: './przeglad.component.html',
  styleUrls: ['./przeglad.component.scss']
})
export class PrzegladComponent implements OnInit {

  zamowienia: ZamowienieRow[] = [];
  cols: any[];

  constructor(private zamowienieService: ZamowienieService, private loginService: LoginService) {
  }

  ngOnInit() {
    if (this.loginService.userHasRole('ROLA_ADMIN')) {
      this.zamowienieService.getZamowienieLista().subscribe(zam => zam ? this.zamowieniaToZamowieniaRow(zam) : this.zamowienia = []);
    } else {
      this.zamowienieService.getZamowienieLista().subscribe(zam => zam ?
        this.zamowieniaToZamowieniaRow(zam.filter(value => {
          return sessionStorage.getItem('username') === value.uzytkownik.username;
        })) : this.zamowienia = []);
    }

    this.cols = [
      {field: 'idZamowienie', header: 'ID'},
      {field: 'komputer', header: 'Komputer'},
      {field: 'osoba', header: 'Osoba'},
      {field: 'data', header: 'Data'}
    ];
  }

  komputerToString(komp: Komputer): string {
    return komp.cpu.nazwa + ' ' + komp.cpu.producent + ', ' +
      komp.gpu.nazwa + ' ' + komp.gpu.producent + ', ' +
      komp.dysk1.nazwa + ' ' + komp.dysk1.producent + ', ' +
      komp.ram.nazwa + ' ' + komp.ram.producent + ', ' +
      komp.zasilacz.nazwa + ' ' + komp.zasilacz.producent + ', ' +
      komp.systemOperacyjny.nazwa + ' ' + komp.systemOperacyjny.producent + ', ';
  }

  zamowieniaToZamowieniaRow(zam: Zamowienie[]) {
    zam.forEach(zamowienie => {
      this.zamowienia.push({
        idZamowienie: zamowienie.idZamowienie,
        komputer: this.komputerToString(zamowienie.komputer),
        osoba: zamowienie.uzytkownik.osoba.imie + ' ' + zamowienie.uzytkownik.osoba.nazwisko,
        data: zamowienie.data
      });
    });
    this.zamowienia = [...this.zamowienia];
  }
}

interface ZamowienieRow {
  idZamowienie: number;
  komputer: string;
  osoba: string;
  data: Date;
}
