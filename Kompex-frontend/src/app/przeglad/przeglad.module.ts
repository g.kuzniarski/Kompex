import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PrzegladComponent} from './przeglad.component';
import {LoginGuard} from '../login/login.guard';
import {DataTableModule} from 'primeng/primeng';
import {ZamowienieService} from '../services/zamowienie.service';
import {LoginService} from '../services/login.service';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule
  ],
  declarations: [PrzegladComponent],
  exports: [PrzegladComponent],
  providers: [ZamowienieService, LoginService, LoginGuard]
})
export class PrzegladModule { }
