import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {
  ButtonModule,
  ConfirmationService,
  ConfirmDialogModule,
  MessagesModule,
  StepsModule,
  TabMenuModule,
  TieredMenuModule,
  ToolbarModule
} from 'primeng/primeng';
import {RouterModule} from '@angular/router';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ImageService} from './services/image.service';
import {AppRoutingModule} from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpModule,
    TieredMenuModule,
    ButtonModule,
    ToolbarModule,
    MessagesModule,
    ConfirmDialogModule,
    FormsModule,
    StepsModule,
    TabMenuModule,
    AppRoutingModule
  ],
  providers: [
    ConfirmationService,
    ImageService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
