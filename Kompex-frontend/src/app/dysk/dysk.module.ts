import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DyskComponent} from './dysk.component';
import {DyskService} from '../services/dysk.service';
import {DataListModule, ButtonModule, GrowlModule} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    DataListModule,
    ButtonModule,
    GrowlModule
  ],
  declarations: [DyskComponent],
  providers: [DyskService],
  exports: [DyskComponent]
})
export class DyskModule {
}
