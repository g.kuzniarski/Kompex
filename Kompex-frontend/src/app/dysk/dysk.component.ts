import {Component, OnInit} from '@angular/core';
import {DyskService} from '../services/dysk.service';
import {Dysk} from '../DBDatatypes';
import {ImageService} from '../services/image.service';
import {DomSanitizer} from '@angular/platform-browser';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-dysk',
  templateUrl: './dysk.component.html',
  styleUrls: ['./dysk.component.scss']
})
export class DyskComponent implements OnInit {

  msgs: Message[] = [];
  private dysks: Dysk[];

  constructor(private dyskService: DyskService, private imageService: ImageService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.dyskService.getDyskLista().subscribe(dysks => {
      this.dysks = dysks;
      this.dysks.forEach(dysk => {
        this.imageService.getImage(dysk.imageName).subscribe(img =>
          dysk.image = this.sanitizer.bypassSecurityTrustUrl('data:image/JPEG;base64,' + img.body));
      });
    });
  }

  onAddPrimaryClick(dysk: Dysk) {
    sessionStorage.setItem('zamowienie.komputer.dysk1.id', dysk.idDysk.toString());
    this.msgs = [];
    this.msgs.push({
      severity: 'success',
      summary: 'Aktualizacja koszyka',
      detail: 'Dodano dysk ' + dysk.producent + ' ' + dysk.nazwa
    });
  }
}
