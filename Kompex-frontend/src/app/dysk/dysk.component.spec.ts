import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DyskComponent } from './dysk.component';

describe('DyskComponent', () => {
  let component: DyskComponent;
  let fixture: ComponentFixture<DyskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DyskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DyskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
