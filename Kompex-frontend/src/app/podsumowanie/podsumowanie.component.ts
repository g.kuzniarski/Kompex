import {Component, EventEmitter, OnInit} from '@angular/core';
import {SystemOperacyjnyService} from '../services/system-operacyjny.service';
import {ZasilaczService} from '../services/zasilacz.service';
import {RAMService} from '../services/ram.service';
import {CPUService} from '../services/cpu.service';
import {GPUService} from '../services/gpu.service';
import {DyskService} from '../services/dysk.service';
import {CPU, Dysk, GPU, RAM, SystemOperacyjny, Zasilacz} from '../DBDatatypes';
import {ZamowienieService} from '../services/zamowienie.service';
import {Router} from '@angular/router';
import {BASE_ROUTE} from '../app-routing.module';

@Component({
  selector: 'app-podsumowanie',
  templateUrl: './podsumowanie.component.html',
  styleUrls: ['./podsumowanie.component.scss']
})
export class PodsumowanieComponent implements OnInit {

  imie: string;
  nazwisko: string;
  soEmitter: EventEmitter<SystemOperacyjny> = new EventEmitter();
  so: SystemOperacyjny;
  dysk1Emitter: EventEmitter<Dysk> = new EventEmitter();
  dysk1: Dysk;
  dysk2Emitter: EventEmitter<Dysk> = new EventEmitter();
  dysk2: Dysk;
  cpuEmitter: EventEmitter<CPU> = new EventEmitter();
  cpu: CPU;
  gpuEmitter: EventEmitter<GPU> = new EventEmitter();
  gpu: GPU;
  ram1Emitter: EventEmitter<RAM> = new EventEmitter();
  ram1: RAM;
  zasEmitter: EventEmitter<Zasilacz> = new EventEmitter();
  zas: Zasilacz;
  isComplete = true;
  suma = 0;


  constructor(private systemOperacyjnyService: SystemOperacyjnyService,
              private dyskService: DyskService,
              private gpuService: GPUService,
              private cpuService: CPUService,
              private ramService: RAMService,
              private zasilaczService: ZasilaczService,
              private zamowienieService: ZamowienieService,
              private router: Router) {
  }

  ngOnInit() {
    this.soEmitter.subscribe(r => this.so = r);
    this.dysk1Emitter.subscribe(r => this.dysk1 = r);
    this.dysk2Emitter.subscribe(r => this.dysk2 = r);
    this.cpuEmitter.subscribe(r => this.cpu = r);
    this.gpuEmitter.subscribe(r => {
      this.gpu = r;
    });
    this.ram1Emitter.subscribe(r => this.ram1 = r);
    this.zasEmitter.subscribe(r => this.zas = r);
    if (sessionStorage.getItem('zamowienie.komputer.systemOperacyjny.id')) {
      this.systemOperacyjnyService.getSystemOperacyjny(+sessionStorage.getItem('zamowienie.komputer.systemOperacyjny.id'))
        .subscribe(value => {
          this.soEmitter.emit(value);
          this.suma += value.cena;
        });
    } else {
      this.isComplete = false;
    }
    if (sessionStorage.getItem('zamowienie.komputer.dysk1.id')) {
      this.dyskService.getDysk(+sessionStorage.getItem('zamowienie.komputer.dysk1.id'))
        .subscribe(value => {
          this.dysk1Emitter.emit(value);
          this.suma += value.cena;
        });
    } else {
      this.isComplete = false;
    }
    if (sessionStorage.getItem('zamowienie.komputer.gpu.id')) {
      this.gpuService.getGPU(+sessionStorage.getItem('zamowienie.komputer.gpu.id'))
        .subscribe(value => {
          this.gpuEmitter.emit(value);
          this.suma += value.cena;
        });
    } else {
      this.isComplete = false;
    }
    if (sessionStorage.getItem('zamowienie.komputer.cpu.id')) {
      this.cpuService.getCPU(+sessionStorage.getItem('zamowienie.komputer.cpu.id'))
        .subscribe(value => {
          this.cpuEmitter.emit(value);
          this.suma += value.cena;
        });
    } else {
      this.isComplete = false;
    }
    if (sessionStorage.getItem('zamowienie.komputer.ram.id')) {
      this.ramService.getRAM(+sessionStorage.getItem('zamowienie.komputer.ram.id'))
        .subscribe(value => {
          this.ram1Emitter.emit(value);
          this.suma += value.cena;
        });
    } else {
      this.isComplete = false;
    }
    if (sessionStorage.getItem('zamowienie.komputer.zasilacz.id')) {
      this.zasilaczService.getZasilacz(+sessionStorage.getItem('zamowienie.komputer.zasilacz.id'))
        .subscribe(value => {
          this.zasEmitter.emit(value);
          this.suma += value.cena;
        });
    } else {
      this.isComplete = false;
    }
    if (sessionStorage.getItem('zamowienie.osoba.imie')) {
      this.imie = sessionStorage.getItem('zamowienie.osoba.imie');
    } else {
      this.isComplete = false;
    }
    if (sessionStorage.getItem('zamowienie.osoba.nazwisko')) {
      this.nazwisko = sessionStorage.getItem('zamowienie.osoba.nazwisko');
    } else {
      this.isComplete = false;
    }
  }

  onZamow() {
    const zamowienie = {
      komputer: {
        systemOperacyjny: this.so,
        zasilacz: this.zas,
        ram: this.ram1,
        dysk1: this.dysk1,
        gpu: this.gpu,
        cpu: this.cpu
      },
      uzytkownik: {
        username: sessionStorage.getItem('username'),
        osoba: {
          imie: this.imie,
          nazwisko: this.nazwisko
        }
      },
      data: new Date()
    };

    this.zamowienieService.createZamowienie(zamowienie).subscribe(r => {
      this.router.navigate([BASE_ROUTE.zamowienie.items[8].routerLink]);
    });
  }
}
