import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PodsumowanieComponent} from './podsumowanie.component';
import {FormsModule} from '@angular/forms';
import {ButtonModule} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ButtonModule
  ],
  declarations: [PodsumowanieComponent],
  exports: [PodsumowanieComponent]
})
export class PodsumowanieModule {
}
