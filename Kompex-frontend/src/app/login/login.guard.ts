import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {BASE_ROUTE} from '../app-routing.module';
import {LoginService} from '../services/login.service';

@Injectable()
export class LoginGuard implements CanActivate {

  constructor(private router: Router,
              private loginService: LoginService) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
     if (!this.loginService.isUserLogged()) {
      this.router.navigate([BASE_ROUTE.login.routerLink]);
      return false;
    }
    return true;
  }
}
