import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login.component';
import {LoginService} from '../services/login.service';
import {ButtonModule, ConfirmDialogModule, InputTextModule, PasswordModule} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    PasswordModule,
    InputTextModule,
    ButtonModule,
    FormsModule,
    ConfirmDialogModule
  ],
  declarations: [LoginComponent],
  exports: [LoginComponent],
  providers: [
    LoginService
  ]
})
export class LoginModule {
}
