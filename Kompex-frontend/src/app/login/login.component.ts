import {Component, OnInit} from '@angular/core';
import {LoginService} from '../services/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  isUserLogged = false;

  constructor(private loginService: LoginService, private router: Router) {
  }

  ngOnInit() {
    this.loginService.userLogged.subscribe(isLogged => {
      this.isUserLogged = isLogged;
    });
    this.isUserLogged = this.loginService.isUserLogged();
    if (this.isUserLogged) {
      this.onLogout();
    }
  }

  onLogin() {
    this.loginService.login(this.username, this.password).subscribe(r => {
      sessionStorage.setItem('token', r.token);
      sessionStorage.setItem('userRole', r.userRole);
      sessionStorage.setItem('username', this.username);
      this.loginService.setUserState();
    });
  }

  onRegister() {
    this.loginService.register(this.username, this.password).subscribe(val => {
      this.loginService.login(this.username, this.password).subscribe(r => {
        sessionStorage.setItem('token', r.token);
        sessionStorage.setItem('userRole', r.userRole);
        this.loginService.setUserState();
      });
    });
  }

  onLogout() {
    this.loginService.logout();
  }
}
