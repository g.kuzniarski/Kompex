import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {KontaktComponent} from './kontakt.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [KontaktComponent],
  exports: [KontaktComponent]
})
export class KontaktModule { }
