import {Component, OnInit} from '@angular/core';
import {ImageService} from './services/image.service';
import {DomSanitizer} from '@angular/platform-browser';
import {Image} from './DBDatatypes';
import {MenuItem} from 'primeng/primeng';
import {ActivatedRoute, Router} from '@angular/router';
import {BASE_ROUTE} from './app-routing.module';
import {LoginService} from './services/login.service';

@Component({
  selector: 'app-kompex-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  logoURL: any;
  private sanitizer: DomSanitizer;
  items: MenuItem[];
  activeItem: MenuItem;

  constructor(private imageService: ImageService, sanitizer: DomSanitizer,
              private router: Router, private route: ActivatedRoute,
              private loginService: LoginService) {
    this.sanitizer = sanitizer;
  }

  ngOnInit(): void {
    this.getLogo();

    const routeItems = Object.values(BASE_ROUTE) as MenuItem[];
    this.items = routeItems.map(i => {
      return {
        label: i.label,
        icon: i.icon,
        routerLink: i.routerLink
      };
    });

    this.items.forEach(item => {
      if (item.routerLink === 'zamowienie' || item.routerLink === 'przeglad') {
        this.loginService.userLogged.subscribe(r => item.disabled = !r);
      } else if (item.label === 'Logowanie' || item.label === 'Wylogowanie') {
        this.loginService.loginButtonLabel.subscribe(r => item.label = r);
        this.loginService.loginButtonIcon.subscribe(r => item.icon = r);
      }
    });

    this.loginService.setUserState();
    this.activeItem = this.items[this.items.findIndex(val => this.router.url.includes(val.routerLink))];
  }

  private getLogo() {
    this.imageService.getImage('kompex-logo-big').subscribe((data: Image) => {
      this.logoURL = this.sanitizer.bypassSecurityTrustUrl('data:image/JPEG;base64,' + data.body);
    });
  }
}
