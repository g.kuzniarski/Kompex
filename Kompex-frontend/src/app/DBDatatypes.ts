export interface Zamowienie {
  idZamowienie?: number;
  komputer: Komputer;
  uzytkownik: Uzytkownik;
  data: Date;
}

export interface Komputer {
  idKomputer?: number;
  cpu: CPU;
  dysk1: Dysk;
  gpu: GPU;
  ram: RAM;
  systemOperacyjny: SystemOperacyjny;
  zasilacz: Zasilacz;
}

export interface Osoba {
  idOsoba?: number;
  imie: string;
  nazwisko: string;
}

export interface Uzytkownik {
  idUzytkownik?: number;
  username: string;
  rola?: string;
  osoba?: Osoba;
}

export interface CPU {
  idCPU?: number;
  nazwa: string;
  producent: string;
  cena: number;
  typGniazda: string;
  iloscRdzeni: number;
  czestotliwoscTaktowania: number;
  pojemnoscL2: number;
  imageName: string;
  image?: any;
}

export interface Dysk {
  idDysk?: number;
  nazwa: string;
  producent: string;
  cena: number;
  pojemnosc: number;
  typ: string;
  interfejs: string;
  imageName: string;
  image?: any;
}

export interface GPU {
  idGPU?: number;
  nazwa: string;
  producent: string;
  cena: number;
  pamiec: number;
  typPamieci: string;
  taktowanie: number;
  typZlacza: string;
  imageName: string;
  image?: any;
}

export interface RAM {
  idRAM?: number;
  nazwa: string;
  producent: string;
  cena: number;
  pojemnosc: number;
  typPamieci: string;
  czestotliwosc: number;
  imageName: string;
  image?: any;
}

export interface SystemOperacyjny {
  idSystemOperacyjny?: number;
  nazwa: string;
  producent: string;
  cena: number;
  imageName: string;
  image?: any;
}

export interface Zasilacz {
  idZasilacz?: number;
  nazwa: string;
  producent: string;
  cena: number;
  moc: number;
  imageName: string;
  image?: any;
}

export interface Podzespol {
  nazwa: string;
  producent: string;
  cena: number;
}

export interface Image {
  body: string;
}
