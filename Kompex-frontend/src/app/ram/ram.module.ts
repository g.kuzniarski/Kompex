import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RAMComponent} from './ram.component';
import {RAMService} from '../services/ram.service';
import {DataListModule, ButtonModule, GrowlModule} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    DataListModule,
    ButtonModule,
    GrowlModule
  ],
  declarations: [RAMComponent],
  providers: [RAMService],
  exports: [RAMComponent]
})
export class RAMModule {
}
