import {Component, OnInit} from '@angular/core';
import {RAMService} from '../services/ram.service';
import {RAM} from '../DBDatatypes';
import {ImageService} from '../services/image.service';
import {DomSanitizer} from '@angular/platform-browser';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-ram',
  templateUrl: './ram.component.html',
  styleUrls: ['./ram.component.scss']
})
export class RAMComponent implements OnInit {

  msgs: Message[] = [];
  private rams: RAM[];

  constructor(private ramService: RAMService, private imageService: ImageService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.ramService.getRAMLista().subscribe(rams => {
      this.rams = rams;
      this.rams.forEach(ram => {
        this.imageService.getImage(ram.imageName).subscribe(img =>
          ram.image = this.sanitizer.bypassSecurityTrustUrl('data:image/JPEG;base64,' + img.body));
      });
      console.log(this.rams);
    });
  }

  onAddPrimaryClick(ram: RAM) {
    sessionStorage.setItem('zamowienie.komputer.ram.id', ram.idRAM.toString());
    this.msgs = [];
    this.msgs.push({
      severity: 'success',
      summary: 'Aktualizacja koszyka',
      detail: 'Dodano pamięć RAM ' + ram.producent + ' ' + ram.nazwa
    });
  }

}
