import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ZamowienieComponent} from './zamowienie/zamowienie.component';
import {DyskComponent} from './dysk/dysk.component';
import {SystemOperacyjnyComponent} from './system-operacyjny/system-operacyjny.component';
import {UzytkownikComponent} from './uzytkownik/uzytkownik.component';
import {CPUComponent} from './cpu/cpu.component';
import {GPUComponent} from './gpu/gpu.component';
import {RAMComponent} from './ram/ram.component';
import {ZasilaczComponent} from './zasilacz/zasilacz.component';
import {PrzegladComponent} from './przeglad/przeglad.component';
import {KontaktComponent} from './kontakt/kontakt.component';
import {PageNotFoundComponent} from './page-not-found.component';
import {PrzegladModule} from './przeglad/przeglad.module';
import {ZamowienieModule} from './zamowienie/zamowienie.module';
import {KontaktModule} from './kontakt/kontakt.module';
import {PodsumowanieComponent} from './podsumowanie/podsumowanie.component';
import {LoginModule} from './login/login.module';
import {LoginComponent} from './login/login.component';
import {LoginGuard} from './login/login.guard';

export const BASE_ROUTE = {
  zamowienie: {
    label: 'Zamówienie',
    routerLink: 'zamowienie',
    icon: 'fa-plus',
    items: [{
      label: 'Użytkownik',
      routerLink: 'zamowienie/uzytkownik',
    }, {
      label: 'System Operacyjny',
      routerLink: 'zamowienie/so',
    }, {
      label: 'Dysk',
      routerLink: 'zamowienie/dysk',
    }, {
      label: 'CPU',
      routerLink: 'zamowienie/cpu',
    }, {
      label: 'GPU',
      routerLink: 'zamowienie/gpu',
    }, {
      label: 'RAM',
      routerLink: 'zamowienie/ram',
    }, {
      label: 'Zasilacz',
      routerLink: 'zamowienie/zasilacz',
    }, {
      label: 'Podsumowanie',
      routerLink: 'zamowienie/podsumowanie',
    }]
  },
  przeglad: {
    label: 'Przeglądaj zamówienia',
    routerLink: 'przeglad',
    icon: 'fa-folder-open'
  },
  kontakt: {
    label: 'Kontakt',
    routerLink: 'kontakt',
    icon: 'fa fa-envelope'
  },
  login: {
    label: 'Logowanie',
    routerLink: 'login',
    icon: 'fa fa-user'
  }
};

const routes: Routes = [
  {
    path: '',
    redirectTo: BASE_ROUTE.login.routerLink,
    pathMatch: 'full'
  },
  {
    path: BASE_ROUTE.zamowienie.routerLink,
    redirectTo: BASE_ROUTE.zamowienie.items[0].routerLink,
    pathMatch: 'full'
  },
  {
    path: BASE_ROUTE.login.routerLink,
    component: LoginComponent
  },
  {
    path: BASE_ROUTE.zamowienie.routerLink,
    component: ZamowienieComponent,
    canActivate: [LoginGuard],
    children: [{
      path: 'dysk',
      component: DyskComponent
    }, {
      path: 'so',
      component: SystemOperacyjnyComponent
    }, {
      path: 'uzytkownik',
      component: UzytkownikComponent
    }, {
      path: 'cpu',
      component: CPUComponent
    }, {
      path: 'gpu',
      component: GPUComponent
    }, {
      path: 'ram',
      component: RAMComponent
    }, {
      path: 'zasilacz',
      component: ZasilaczComponent
    }, {
      path: 'podsumowanie',
      component: PodsumowanieComponent
    }]
  },
  {
    path: BASE_ROUTE.przeglad.routerLink,
    component: PrzegladComponent,
    canActivate: [LoginGuard]
  },
  {
    path: BASE_ROUTE.kontakt.routerLink,
    component: KontaktComponent
  }, {
    path: BASE_ROUTE.login.routerLink,
    component: LoginComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    ZamowienieModule,
    KontaktModule,
    PrzegladModule,
    LoginModule
  ],
  exports: [RouterModule],
  declarations: [PageNotFoundComponent],
  providers: []
})
export class AppRoutingModule {
}
