import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-page-not-found',
  template: `
    <h3>Page not found</h3>
  `
})
export class PageNotFoundComponent implements OnInit {

  message: string;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.message = this.route.snapshot.params['message'];
  }

}

