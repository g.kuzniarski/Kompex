import {Component, OnInit} from '@angular/core';
import {SystemOperacyjny} from '../DBDatatypes';
import {SystemOperacyjnyService} from '../services/system-operacyjny.service';
import {ImageService} from '../services/image.service';
import {DomSanitizer} from '@angular/platform-browser';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-system-operacyjny',
  templateUrl: './system-operacyjny.component.html',
  styleUrls: ['./system-operacyjny.component.scss']
})
export class SystemOperacyjnyComponent implements OnInit {

  msgs: Message[] = [];
  private systemOperacyjnys: SystemOperacyjny[];

  constructor(private systemOperacyjnyService: SystemOperacyjnyService, private imageService: ImageService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.systemOperacyjnyService.getSystemOperacyjnyLista().subscribe(systemOperacyjnys => {
      this.systemOperacyjnys = systemOperacyjnys;
      this.systemOperacyjnys.forEach(systemOperacyjny => {
        this.imageService.getImage(systemOperacyjny.imageName).subscribe(img =>
          systemOperacyjny.image = this.sanitizer.bypassSecurityTrustUrl('data:image/JPEG;base64,' + img.body));
      });
    });
  }

  onAddPrimaryClick(systemOperacyjny: SystemOperacyjny) {
    sessionStorage.setItem('zamowienie.komputer.systemOperacyjny.id', systemOperacyjny.idSystemOperacyjny.toString());
    this.msgs = [];
    this.msgs.push({
      severity: 'success',
      summary: 'Aktualizacja koszyka',
      detail: 'Dodano system ' + systemOperacyjny.producent + ' ' + systemOperacyjny.nazwa
    });
  }
}
