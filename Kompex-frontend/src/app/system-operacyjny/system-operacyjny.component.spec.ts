import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemOperacyjnyComponent } from './system-operacyjny.component';

describe('SystemOperacyjnyComponent', () => {
  let component: SystemOperacyjnyComponent;
  let fixture: ComponentFixture<SystemOperacyjnyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemOperacyjnyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemOperacyjnyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
