import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SystemOperacyjnyService} from '../services/system-operacyjny.service';
import {SystemOperacyjnyComponent} from './system-operacyjny.component';
import {ButtonModule, DataListModule, GrowlModule} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    DataListModule,
    ButtonModule,
    GrowlModule
  ],
  declarations: [SystemOperacyjnyComponent],
  providers: [SystemOperacyjnyService],
  exports: [SystemOperacyjnyComponent]
})
export class SystemOperacyjnyModule {
}
