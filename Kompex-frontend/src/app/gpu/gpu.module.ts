import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GPUComponent} from './gpu.component';
import {GPUService} from '../services/gpu.service';
import {DataListModule, ButtonModule, GrowlModule} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    DataListModule,
    ButtonModule,
    GrowlModule
  ],
  declarations: [GPUComponent],
  providers: [GPUService],
  exports: [GPUComponent]
})
export class GPUModule {
}
