import {Component, OnInit} from '@angular/core';
import {GPUService} from '../services/gpu.service';
import {GPU} from '../DBDatatypes';
import {ImageService} from '../services/image.service';
import {DomSanitizer} from '@angular/platform-browser';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-gpu',
  templateUrl: './gpu.component.html',
  styleUrls: ['./gpu.component.scss']
})
export class GPUComponent implements OnInit {

  msgs: Message[] = [];
  private gpus: GPU[];

  constructor(private gpuService: GPUService, private imageService: ImageService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.gpuService.getGPULista().subscribe(gpus => {
      this.gpus = gpus;
      this.gpus.forEach(gpu => {
        this.imageService.getImage(gpu.imageName).subscribe(img =>
          gpu.image = this.sanitizer.bypassSecurityTrustUrl('data:image/JPEG;base64,' + img.body));
      });
    });
  }

  onAddPrimaryClick(gpu: GPU) {
    sessionStorage.setItem('zamowienie.komputer.gpu.id', gpu.idGPU.toString());
    this.msgs = [];
    this.msgs.push({
      severity: 'success',
      summary: 'Aktualizacja koszyka',
      detail: 'Dodano kartę graficzną ' + gpu.producent + ' ' + gpu.nazwa
    });
  }

}
