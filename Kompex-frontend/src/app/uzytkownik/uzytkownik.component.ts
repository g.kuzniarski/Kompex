import {Component, OnInit} from '@angular/core';
import {Message} from 'primeng/primeng';
import {Osoba, Zamowienie} from '../DBDatatypes';

@Component({
  selector: 'app-uzytkownik',
  templateUrl: './uzytkownik.component.html',
  styleUrls: ['./uzytkownik.component.scss']
})
export class UzytkownikComponent implements OnInit {

  constructor() {
  }

  imie: string;
  nazwisko: string;
  msgs: Message[] = [];

  ngOnInit() {
    this.imie = sessionStorage.getItem('zamowienie.osoba.imie');
    this.nazwisko = sessionStorage.getItem('zamowienie.osoba.nazwisko');
  }

  onSave() {
    sessionStorage.setItem('zamowienie.osoba.imie', this.imie);
    sessionStorage.setItem('zamowienie.osoba.nazwisko', this.nazwisko);
    this.msgs = [];
    this.msgs.push({
      severity: 'success',
      summary: 'Aktualizacja danych',
      detail: 'Dane: ' + this.imie + ' ' + this.nazwisko
    });
  }
}
