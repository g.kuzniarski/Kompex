import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UzytkownikComponent} from './uzytkownik.component';
import {UzytkownikService} from '../services/uzytkownik.service';
import {DataListModule, ButtonModule, GrowlModule, InputTextModule} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    DataListModule,
    ButtonModule,
    InputTextModule,
    ButtonModule,
    FormsModule,
    GrowlModule
  ],
  declarations: [UzytkownikComponent],
  providers: [UzytkownikService],
  exports: [UzytkownikComponent]
})
export class UzytkownikModule {
}
