import {Component, OnInit} from '@angular/core';
import {CPUService} from '../services/cpu.service';
import {CPU} from '../DBDatatypes';
import {ImageService} from '../services/image.service';
import {DomSanitizer} from '@angular/platform-browser';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'app-cpu',
  templateUrl: './cpu.component.html',
  styleUrls: ['./cpu.component.scss']
})
export class CPUComponent implements OnInit {

  msgs: Message[] = [];
  private cpus: CPU[];

  constructor(private cpuService: CPUService, private imageService: ImageService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.cpuService.getCPULista().subscribe(cpus => {
      this.cpus = cpus;
      this.cpus.forEach(cpu => {
        this.imageService.getImage(cpu.imageName).subscribe(img =>
          cpu.image = this.sanitizer.bypassSecurityTrustUrl('data:image/JPEG;base64,' + img.body));
      });
    });
  }

  onAddPrimaryClick(cpu: CPU) {
    sessionStorage.setItem('zamowienie.komputer.cpu.id', cpu.idCPU.toString());
    this.msgs = [];
    this.msgs.push({
      severity: 'success',
      summary: 'Aktualizacja koszyka',
      detail: 'Dodano procesor ' + cpu.producent + ' ' + cpu.nazwa
    });
  }

}
