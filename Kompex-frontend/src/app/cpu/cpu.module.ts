import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CPUComponent} from './cpu.component';
import {CPUService} from '../services/cpu.service';
import {DataListModule, ButtonModule, GrowlModule} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    DataListModule,
    ButtonModule,
    GrowlModule
  ],
  declarations: [CPUComponent],
  providers: [CPUService],
  exports: [CPUComponent]
})
export class CPUModule {
}
