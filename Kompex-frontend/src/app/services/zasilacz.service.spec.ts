import { TestBed, inject } from '@angular/core/testing';

import { ZasilaczService } from './zasilacz.service';

describe('ZasilaczService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ZasilaczService]
    });
  });

  it('should be created', inject([ZasilaczService], (service: ZasilaczService) => {
    expect(service).toBeTruthy();
  }));
});
