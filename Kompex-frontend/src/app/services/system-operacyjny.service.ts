import {Injectable} from '@angular/core';
import {FetchApiService} from './fetch-api.service';
import {Observable} from 'rxjs/Observable';
import {SystemOperacyjny} from '../DBDatatypes';

import 'rxjs/add/observable/of';

const SYSTEM_OPERACYJNY_URL = '/api/systemOperacyjny';

@Injectable()
export class SystemOperacyjnyService extends FetchApiService {

    getSystemOperacyjnyLista(): Observable<SystemOperacyjny[]> {
        return this.get(`${SYSTEM_OPERACYJNY_URL}s/`);
    }

    getSystemOperacyjny(idSystemOperacyjny: number): Observable<SystemOperacyjny> {
        return this.get(`${SYSTEM_OPERACYJNY_URL}/${idSystemOperacyjny}`);
    }

    createSystemOperacyjny(systemOperacyjny: SystemOperacyjny) {
        return this.post(`${SYSTEM_OPERACYJNY_URL}`, systemOperacyjny);
    }

    delSystemOperacyjny(idSystemOperacyjny: number) {
        return this.del(`${SYSTEM_OPERACYJNY_URL}/remove/${idSystemOperacyjny}`);
    }

    updateSystemOperacyjny(systemOperacyjny: SystemOperacyjny) {
        return this.put(`${SYSTEM_OPERACYJNY_URL}/${systemOperacyjny.idSystemOperacyjny}`, systemOperacyjny);
    }
}
