import {Injectable} from '@angular/core';
import {FetchApiService} from './fetch-api.service';
import {Observable} from 'rxjs/Observable';
import {Dysk} from '../DBDatatypes';

import 'rxjs/add/observable/of';

const DYSK_URL = '/api/dysk';

@Injectable()
export class DyskService extends FetchApiService {

  getDyskLista(): Observable<Dysk[]> {
    return this.get(`${DYSK_URL}s/`);
  }

  getDysk(idDysk: number): Observable<Dysk> {
    return this.get(`${DYSK_URL}/${idDysk}`);
  }

  createDysk(dysk: Dysk) {
    return this.post(`${DYSK_URL}`, dysk);
  }

  delDysk(idDysk: number) {
    return this.del(`${DYSK_URL}/remove/${idDysk}`);
  }

  updateDysk(dysk: Dysk) {
    return this.put(`${DYSK_URL}/${dysk.idDysk}`, dysk);
  }
}
