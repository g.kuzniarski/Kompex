import { TestBed, inject } from '@angular/core/testing';

import { DyskService } from './dysk.service';

describe('DyskService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DyskService]
    });
  });

  it('should be created', inject([DyskService], (service: DyskService) => {
    expect(service).toBeTruthy();
  }));
});
