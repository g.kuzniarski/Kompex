import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {Http, Response} from '@angular/http';
import {Image} from '../DBDatatypes';

const IMAGE_URL = '/api/image';

@Injectable()
export class ImageService {
  constructor(public http: Http) {
  }

  getImage(imageName: string): Observable<Image> {
    return this.http.get(`${IMAGE_URL}/${imageName}`)
      .map((response: Response) => {
        let image: Image;
        image = {
          body: response.text()
        };
        return image;
      });
  }
}
