import {EventEmitter, Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {FetchApiService} from './fetch-api.service';
import {Router} from '@angular/router';
import {BASE_ROUTE} from '../app-routing.module';

const LOGIN_PATH = '/api/login';
const REGISTER_PATH = '/api/register';
const UZYTKOWNIK_PATH = '/api/uzytkownik';

@Injectable()
export class LoginService extends FetchApiService {
  userLogged: EventEmitter<boolean> = new EventEmitter();
  loginButtonLabel: EventEmitter<string> = new EventEmitter();
  loginButtonIcon: EventEmitter<string> = new EventEmitter();

  constructor(public http: Http,
              private router: Router) {
    super(http);
  }

  login(username: string, password: string) {
    const body = {
      username: username,
      password: password,
    };
    return this.post(LOGIN_PATH, body);
  }

  register(username: string, password: string) {
    const body = {
      username: username,
      password: password,
    };
    return this.post(REGISTER_PATH, body);
  }

  logout() {
    sessionStorage.clear();
    this.setUserState();
    this.router.navigate([BASE_ROUTE.login.routerLink]);
  }

  setUserState() {
    if (this.isUserLogged()) {
      this.userLogged.emit(true);
      this.loginButtonLabel.emit('Wylogowanie');
      this.loginButtonIcon.emit('fa-sign-out');
    } else {
      this.userLogged.emit(false);
      this.loginButtonLabel.emit('Logowanie');
      this.loginButtonIcon.emit('fa-plus');
    }
  }

  isUserLogged(): boolean {
    return !!(sessionStorage.getItem('token') && sessionStorage.getItem('userRole'));
  }

  userHasRole(role: string): boolean {
    return sessionStorage.getItem('userRole') === role;
  }
}
