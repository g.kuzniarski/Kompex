import {Injectable} from '@angular/core';
import {FetchApiService} from './fetch-api.service';
import {Observable} from 'rxjs/Observable';
import {Zasilacz} from '../DBDatatypes';

import 'rxjs/add/observable/of';

const ZASILACZ_URL = '/api/zasilacz';

@Injectable()
export class ZasilaczService extends FetchApiService {

  getZasilaczLista(): Observable<Zasilacz[]> {
    return this.get(`${ZASILACZ_URL}s/`);
  }

  getZasilacz(idZasilacz: number): Observable<Zasilacz> {
    return this.get(`${ZASILACZ_URL}/${idZasilacz}`);
  }

  createZasilacz(zasilacz: Zasilacz) {
    return this.post(`${ZASILACZ_URL}`, zasilacz);
  }

  delZasilacz(idZasilacz: number) {
    return this.del(`${ZASILACZ_URL}/remove/${idZasilacz}`);
  }

  updateZasilacz(zasilacz: Zasilacz) {
    return this.put(`${ZASILACZ_URL}/${zasilacz.idZasilacz}`, zasilacz);
  }
}
