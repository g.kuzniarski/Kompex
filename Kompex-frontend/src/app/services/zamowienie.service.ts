import {Injectable} from '@angular/core';
import {FetchApiService} from './fetch-api.service';
import {Observable} from 'rxjs/Observable';
import {Zamowienie} from '../DBDatatypes';

import 'rxjs/add/observable/of';

const ZAMOWIENIE_URL = '/api/zamowienie';

@Injectable()
export class ZamowienieService extends FetchApiService {

  getZamowienieLista(): Observable<Zamowienie[]> {
    return this.get(`${ZAMOWIENIE_URL}s/`);
  }

  getZamowienie(idZamowienie: number): Observable<Zamowienie> {
    return this.get(`${ZAMOWIENIE_URL}/${idZamowienie}`);
  }

  createZamowienie(zamowienie: Zamowienie) {
    return this.post(`${ZAMOWIENIE_URL}/`, zamowienie);
  }

  delZamowienie(idZamowienie: number) {
    return this.del(`${ZAMOWIENIE_URL}/remove/${idZamowienie}`);
  }

  updateZamowienie(zamowienie: Zamowienie) {
    return this.put(`${ZAMOWIENIE_URL}/${zamowienie.idZamowienie}`, zamowienie);
  }
}
