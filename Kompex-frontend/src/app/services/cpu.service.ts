import {Injectable} from '@angular/core';
import {FetchApiService} from './fetch-api.service';
import {Observable} from 'rxjs/Observable';
import {CPU} from '../DBDatatypes';

import 'rxjs/add/observable/of';

const CPU_URL = '/api/cpu';

@Injectable()
export class CPUService extends FetchApiService {

  getCPULista(): Observable<CPU[]> {
    return this.get(`${CPU_URL}s/`);
  }

  getCPU(idCPU: number): Observable<CPU> {
    return this.get(`${CPU_URL}/${idCPU}`);
  }

  createCPU(cpu: CPU) {
    return this.post(`${CPU_URL}`, cpu);
  }

  delCPU(idCPU: number) {
    return this.del(`${CPU_URL}/remove/${idCPU}`);
  }

  updateCPU(cpu: CPU) {
    return this.put(`${CPU_URL}/${cpu.idCPU}`, cpu);
  }
}
