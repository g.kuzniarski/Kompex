import {Injectable} from '@angular/core';
import {FetchApiService} from './fetch-api.service';
import {Observable} from 'rxjs/Observable';
import {Osoba} from '../DBDatatypes';

import 'rxjs/add/observable/of';

const OSOBA_URL = '/api/osoba';

@Injectable()
export class OsobaService extends FetchApiService {

  getOsobaLista(): Observable<Osoba[]> {
    return this.get(`${OSOBA_URL}s/`);
  }

  getOsoba(idOsoba: number): Observable<Osoba> {
    return this.get(`${OSOBA_URL}/${idOsoba}`);
  }

  createOsoba(osoba: Osoba) {
    return this.post(`${OSOBA_URL}`, osoba);
  }

  delOsoba(idOsoba: number) {
    return this.del(`${OSOBA_URL}/remove/${idOsoba}`);
  }

  updateOsoba(osoba: Osoba) {
    return this.put(`${OSOBA_URL}/${osoba.idOsoba}`, osoba);
  }
}
