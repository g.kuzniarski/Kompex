import { TestBed, inject } from '@angular/core/testing';

import { SystemOperacyjnyService } from './system-operacyjny.service';

describe('SystemOperacyjnyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SystemOperacyjnyService]
    });
  });

  it('should be created', inject([SystemOperacyjnyService], (service: SystemOperacyjnyService) => {
    expect(service).toBeTruthy();
  }));
});
