import {Injectable} from '@angular/core';
import {FetchApiService} from './fetch-api.service';
import {Observable} from 'rxjs/Observable';
import {RAM} from '../DBDatatypes';

import 'rxjs/add/observable/of';

const RAM_URL = '/api/ram';

@Injectable()
export class RAMService extends FetchApiService {

  getRAMLista(): Observable<RAM[]> {
    return this.get(`${RAM_URL}s/`);
  }

  getRAM(idRAM: number): Observable<RAM> {
    return this.get(`${RAM_URL}/${idRAM}`);
  }

  createRAM(ram: RAM) {
    return this.post(`${RAM_URL}`, ram);
  }

  delRAM(idRAM: number) {
    return this.del(`${RAM_URL}/remove/${idRAM}`);
  }

  updateRAM(ram: RAM) {
    return this.put(`${RAM_URL}/${ram.idRAM}`, ram);
  }
}
