import {Injectable} from '@angular/core';
import {FetchApiService} from './fetch-api.service';
import {Observable} from 'rxjs/Observable';
import {Uzytkownik} from '../DBDatatypes';

import 'rxjs/add/observable/of';

const UZYTKOWNIK_URL = '/api/uzytkownik';

@Injectable()
export class UzytkownikService extends FetchApiService {

  getUzytkownikLista(): Observable<Uzytkownik[]> {
    return this.get(`${UZYTKOWNIK_URL}s/`);
  }

  getUzytkownik(idUzytkownik: number): Observable<Uzytkownik> {
    return this.get(`${UZYTKOWNIK_URL}/${idUzytkownik}`);
  }

  createUzytkownik(uzytkownik: Uzytkownik) {
    return this.post(`${UZYTKOWNIK_URL}`, uzytkownik);
  }

  delUzytkownik(idUzytkownik: number) {
    return this.del(`${UZYTKOWNIK_URL}/remove/${idUzytkownik}`);
  }

  updateUzytkownik(uzytkownik: Uzytkownik) {
    return this.put(`${UZYTKOWNIK_URL}/${uzytkownik.idUzytkownik}`, uzytkownik);
  }
}
