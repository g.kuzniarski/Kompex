import {Injectable} from '@angular/core';
import {FetchApiService} from './fetch-api.service';
import {Observable} from 'rxjs/Observable';
import {GPU} from '../DBDatatypes';

import 'rxjs/add/observable/of';

const GPU_URL = '/api/gpu';

@Injectable()
export class GPUService extends FetchApiService {

  getGPULista(): Observable<GPU[]> {
    return this.get(`${GPU_URL}s/`);
  }

  getGPU(idGPU: number): Observable<GPU> {
    return this.get(`${GPU_URL}/${idGPU}`);
  }

  createGPU(gpu: GPU) {
    return this.post(`${GPU_URL}`, gpu);
  }

  delGPU(idGPU: number) {
    return this.del(`${GPU_URL}/remove/${idGPU}`);
  }

  updateGPU(gpu: GPU) {
    return this.put(`${GPU_URL}/${gpu.idGPU}`, gpu);
  }
}
